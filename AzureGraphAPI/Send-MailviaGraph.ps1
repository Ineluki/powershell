$upn = "" ;

try {
    $params @{
        Message = @{
            Subject = "Test"
            Body = @{
                ContentType = "HTML"
                Content = "Hallo, <br> <br> dies ist ein <b> Test </b> ! <br> <br>"
            }
            ToRecipients = @{
                @{
                    EmailAddress = @{
                        Address = "myself@me.de"
                    }
                }
            }
            CCRecipients = @{
                EmailAddress = @{
                    Address = "otherpeople@away.com"
                }
            }
        }
        SaveToSentItems = "false"
    }
    Send-MgUserMail -UserId $upn -Bodyparameter $params ;
}
catch {
    <#Do this if a terminating exception happens#>
    Write-Output $error
}