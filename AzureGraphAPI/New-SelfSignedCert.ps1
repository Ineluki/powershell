#Create a self signed certificate
$subject_certname = "GraphAPI-Cert-XYZ" ;
$certstorelocation = "Cert:\LocalMachine\My" ;
$cert = New-SelfSignedCertificate -Subject "CN=$subject_certname" -CertStoreLocation $certstorelocation -KeyExportPolicy Exportable -KeySpec Signature -KeyLength 2048 -KeyAlgorithm RSA -HashAlgorithm SHA256 ;

#Expot public key as CER for upload in Azure 
$cert_exportpath = "C:\Temp\$subject_certname.cer" ;
Export-Certificate -Cert $cert -FilePath $cert_exportpath ;

#Create a secure password
$passwordmanagerpwd = Read-Host -Prompt "Enter a secure password created in a password manager: " -AsSecureString ;
$certpwd = ConvertTo-SecureString -String $passwordmanagerpwd -Force -AsPlainText ;

#Export as PFX
$cert_exportpath_pfx = "C:\Temp\$subject_certname.pfx" ;
Export-PfxCertificate -Cert $cert -FilePath $cert_exportpath_pfx -Password $certpwd ;