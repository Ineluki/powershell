#Test tokens here: https://adfshelp.microsoft.com/JwtDecoder/GetToken
#Provide Azure Tenant ID
$tenantid = "" ;
#Provide application ID
$appid = "" ;
#Provide secret value (NOT THE SECRET ID!!!)
$secretvalue = "" ;

$graphuri = "https://graph.microsoft.com" ;
$authuri = "https://login.microsoftonline.com/$tenantid/oauth2/token" ;
$authbody = [Ordered] @{
    resource = "$graphuri"
    client_id = "$appid"
    client_secret = "$secretvalue"
    grant_type = 'client_credentials'
}

$auth_process = Invoke-RestMethod -Method Post -Uri $authuri -Body $authbody -ErrorAction Stop ;
$token = $auth_process.access_token ;
$token ;

Connect-MgGraph -AccessToken ($token | ConvertTo-SecureString -AsPlainText -Force) ;
Get-MgContext ;
(Get-MgContext).Scopes ;