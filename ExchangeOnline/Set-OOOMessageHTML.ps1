#Set Oot-Of-Office-Notification with HTML template

#Get the mailbox for which OOF will be set

$emailaddress = "" ;

$mailbox = Get-Mailbox $emailaddress ;

#Define body of email
$emailbody = @"
<html> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"
    </head> 
    <body style="font-family: Arial; font-size:14;">

        Sehr geehrte Damen und Herren,
        <br>
        <br>
        in der Zeit vom 01.01.23 bis 12.01.23 kann ich Ihre Anfrage leider nicht bearbeiten. <br>
        <br>
        Im Falle eines dringenden Anliegens wenden Sie sich bitte an meine Kollegen, diese erreichen Sie unter folgenden Kontaktdaten: <br>
        <br>
        Name: <br>
        Telefon: <br>
        Email: <a href="mailto=meine.kollegin@firma.de">meine.kollegin@firma.de </a> <br>
        <br>
        <br>
        Vielen Dank für Ihr Verständnis! <br>
        <br>
        Mit freundlichen Grüßen <br>
        <br>
        Toni Tester <br>
        <br>
            <p style="font-size=11">
            Dipl.-Kffm. Biochemie <br>
            Finanzbuchhaltung <br>
            <br>
            </p>

    </body>
</html>
"@

#Activate Autoreply
$mailbox | Set-MailboxAutoReplyConfiguration -AutoReplyState Enabled -InternalMessage $emailbody -ExternalMessage $emailbody ;