function Convert-MTLReport {
<# ;
    .SYNOPSIS
        Converts the Message Trace Reports form Microsoft O365 Exchange Online Portal into a human-readable version which can be analyzed in Excel. 
    .DESCRIPTION
        Convert-MTLReport is a function that converts CSV files into a human-readable version.
    .EXAMPLE
        Convert-MTLReport -Sourcepath "C:\Temp\MTL21.csv" -Targetpath "C:\Temp\ConvertedMTLcsv.csv" ;
    .INPUTS 
        CSV file  
    .OUTPUTS
        CSV file  
    .NOTES
        Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute 
        Author: Ineluki 
        Message: GUI fails, Powershell prevails! 
#>
    [CmdletBinding()]
    param ( 
        [Parameter()] 
        [string]
        $Sourcepath,

        [Parameter()] 
        [string]
        $Targetpath
    ) 

    ##Check if C:\Temp exists, create it otherwise
    if ((Test-Path -Path "C:\Temp") -ne $true) {
        New-Item -ItemType Directory -Path "C:\Temp" ;   
    }

    #Create a timestamp
    $timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

    #Set location to C:\Temp
    Set-Location -Path "C:\Temp" ;

    #Create backup of the csv file
    $filecopy_path = "C:\Temp\MTL_Backup_" ;
    $filecopy_path += $timestamp ;
    $filecopy_path += ".csv" ;

    Copy-Item -Path $Sourcepath -Destination $filecopy_path ;

    #Save the copy as TXT file - fixes Import-/Export-Csv-cmdlet problems
    $filecopy_path_txt = $filecopy_path.Replace("csv","txt") ;
    Rename-Item -Path $filecopy_path -NewName $filecopy_path_txt ;

    #Import and export file again to make it human-readable in Excel
    $report = Import-Csv -Path  $filecopy_path_txt ;
    $report | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path $Targetpath ;
}
