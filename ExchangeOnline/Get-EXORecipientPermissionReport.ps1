    <#Ensure that Exchange commandlets are available
    #Load PSSnapin for Exchange2007
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
    #Load PSSnapin for Exchange2010
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
    #Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
    #>
    #Set the scope of commandlets to entire forest
    Set-AdServerSettings -ViewEntireForest $true ;

$exorecipients = Get-Recipient -ResultSize Unlimited -Filter 'Name -ne "DiscoverySearchMailbox{D919BA05-46A6-415f-80AD-7E09334BB852}"' ;
$exorecipientpermission = $exorecipients | Get-EXORecipientPermission | Where-Object {$_.Trustee -ne "NT AUTHORITY\SELF"} ;
$exorecipientpermission | Select-Object Identity, Trustee, @{Name='AccessRights';Expression={$_.AccessRights -join ","}} ;
