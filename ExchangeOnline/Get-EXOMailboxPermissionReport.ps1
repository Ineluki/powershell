    <#Ensure that Exchange commandlets are available
    #Load PSSnapin for Exchange2007
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
    #Load PSSnapin for Exchange2010
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
    #Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
    #>
    #Set the scope of commandlets to entire forest
    Set-AdServerSettings -ViewEntireForest $true ;

$exomailboxes = Get-EXOMailbox -ResultSize Unlimited -Filter 'Name -ne "DiscoverySearchMailbox{D919BA05-46A6-415f-80AD-7E09334BB852}"' ;
$exopermissions = $exomailboxes | Get-ExoMailboxPermission | Where-Object {$_.User -ne "NT AUTHORITY\SELF"} ;
$exopermissions | Select-Object Identity, User, @{Name='AccessRights';Expression={$_.AccessRights -join ","}} ; 