
$csv_exportpath = "C:\Temp\softdeletedmailboxes.csv"
Get-Mailbox -SoftDeletedMailbox -ResultSize Unlimited | Select-Object PrimarySmtpAddress,Displayname,Alias,WhenMailboxCreated,WhenSoftDeleted,HiddenFromAdressListEnabled | Export-Csv -NoTypeInformation -Encoding UTF8 -Delimiter ";" -Path $csv_exportpath ;