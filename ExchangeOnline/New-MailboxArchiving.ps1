#Check https://compliance.microsoft.com -> "Data lifecycle management" -> "Microsoft 365"  -> "Retention Policies"
#Create tags, create policy, add tags to policy, apply policy to user mailbox, enable archive, start folder assistant
$mailbox = "" ;
$policyname = "Default-Archiving-Policy" ;

New-RetentionPolicyTag -Name "24MonthsMoveToArchive" -Type "All" -Comment "Move to archive after 24 months" -RetentionEnabled $true -AgeLimitForRetention "730" -RetentionAction "MoveToArchive" ;
New-RetentionPolicyTag -Name "30DaysDeleteButAllowRecovery" -Type "DeletedItems" -Comment "Deletes 'Deleted Items' folder after 30 days, but allows recovery" -RetentionEnabled $true -AgeLimitForRetention "30" -RetentionAction "DeleteAndAllowRecovery" ;
New-RetentionPolicyTag -Name "7DaysMoveRecoverableItemsToArchive" -Type "RecoverableItems" -Comment "Recoverable items are moved to archive after 7 days" -RetentionEnabled $true -AgeLimitForRetention "7" -RetentionAction "MoveToArchive" ;

New-RetentionPolicy -Name $policyname ;
Get-RetentionPolicy -Identity $policyname ;
Get-RetentionPolicy -Identity $policyname | Set-RetentionPolicy -RetentionPolicyTags "24MonthsMoveToArchive","30DaysDeleteButAllowRecovery","7DaysMoveRecoverableItemsToArchive" ;

Get-Mailbox -Identity $mailbox | Format-Table PrimarySMTPAddress,RetentionPolicy ;
Get-Mailbox -Identity $mailbox | Set-Mailbox -RetentionPolicy $policyname ;
Get-Mailbox -Identity $mailbox | Enable-Mailbox -Archive ;
Start-ManagedFolderAssistant -Identity $mailbox ;

