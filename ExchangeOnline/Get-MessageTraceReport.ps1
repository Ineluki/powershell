
#Run the short version - report from 10 days in the past maximum
#Set variables
$senderaddress = "" ;
$recipientaddress = "" ;

#Calculate dates
$numberofdays = "10" ;
$startdate = ((Get-Date).AddDays(-($numberofdays))) ;
$enddate = Get-Date ;

#Run the query
Get-MessageTrace -SenderAddress $senderaddress -RecipientAddress $recipientaddress -StartDate $startdate -Enddate $enddate ;

#Run the long version - report report from 90 days in the past maximum
#Set variables
$senderaddress = "" ;
$recipientaddress= "" ;
$reporttitle = "TicketXYZ" ;
$reporttype = "MessageTraceDetail"


#Calculate dates
$numberofdays = "90" ;
$startdate = ((Get-Date).AddDays(-($numberofdays))) ;
$enddate = Get-Date ;

Start-HistoricalSearch -SenderAddress $senderaddress -RecipientAdddress $recipientaddress -StartDate $startdate -Enddate $enddate -ReportTitle $reporttitle -ReportType $reporttype ;