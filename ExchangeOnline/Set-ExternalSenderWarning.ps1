Connect-ExchangeOnline;

Get-ExternalInOutlook | Format-List * ;
Set-ExternalInOutlook -Enabled $true ;

#Add domains to allow list
#Set-ExternalInOutlook -AllowList @{Add="steve.com"}

Get-TransportRule | Format-Table Identity,State,Priority,Comments ;

###############################
###Adjust according to needs###
###############################

$comment = "Ticket" ;
$name = "ExternalSender-RedWarningBanner" ;
$fromscope = "NotInOrganization" ;
$auditseverity = "High" ;
$applyhtmldisclaimerlocation = "Prepend" ;
$applyhtmldisclaimerfallbackaction = "Wrap" ;
$stopruleprocessing = $false ;
$priority = "0" ;
$senderaddresslocation = "HeaderOrEnvelope" ;
$enabled = $false ;
$applyhtmldisclaimertext = @"
<!-- Red Banner -->
<table border=0 cellspacing=0 cellpadding=0 align="left" width="100%">
  <tr>
    <td style="background:#ff0000;padding:5pt 2pt 5pt 2pt"></td>
    <td width="100%" cellpadding="7px 6px 7px 15px" style="background:#ff000;padding:5pt 4pt 5pt 12pt;word-wrap:break-word">
      <div style="color:#000000;">
        <span style="color:#000000; font-weight:bold;">CAUTION, this is an external email and may be malicious!!!</span>
        Please take care when clicking links or opening attachments.
      </div>
    </td>
  </tr>
</table>
<br />
"@

#Create new rule
New-Transportrule -Name $name -Priority $priority -FromScope $fromscope -Enabled $enabled -SetAuditSeverity $auditseverity -SenderAddressLocation $senderaddresslocation -ApplyHtmlDisclaimerLocation $applyhtmldisclaimerlocation -ApplyHtmlDisclaimerText $applyhtmldisclaimertext -ApplyHtmlDisclaimerFallbackAction $applyhtmldisclaimerfallbackaction -StopRuleProcessing $stopruleprocessing -Comments $comment ;
Enable-Transportrule -Identity $name ;