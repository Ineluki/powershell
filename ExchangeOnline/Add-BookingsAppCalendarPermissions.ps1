#Microsoft Bookings App 

#Show all Booking calendars
Get-EXOMailbox -RecipientTypeDetails SchedulingMailbox -Resultsize Unlimited | Format-Table DisplayName,PrimarySmtpAddress,EmailAddresses ;

#Set variables for later use
$bookingcalendar_mailboxaddress = "" ;
$bookinguser = "" ;

#Set permissions for FullAccess and SendAs
Add-MailboxPermission -Identity $bookingcalendar_mailboxaddress -User $bookinguser -AccessRights FullAccess -Deny:$false ;
Add-RecipientPermissions -Identity $bookingcalendar_mailboxaddress -Trustee $bookinguser -AccessRights SendAs -Confirm:$false ;

#Delete Booking calendar
Remove-Mailbox -Identity $bookingcalendar_mailboxaddress ;