$csv_exportpath_mailboxretention = "C:\Temp\mailboxdeleteditemsretentionperiod.csv" ;
Get-Mailbox -ResultSize Unlimited | Select-Object PrimarySmtpAddress,RetainDeletedItemsFor,UseDatabaseRetentionDefaults,RetainDeletedItemsUntilBackup | Export-Csv -NoTypeInformation -Encoding UTF8 -Delimiter ";" -Path $csv_exportpath_mailboxretention ;

$csv_exportpath_mailboxplanretention = "C:\Temp\mailboxplanretentionperiod.csv" ;
Get-MailboxPlan | Select-Object Name,RetainDeletedItemsFor | Export-Csv -NoTypeInformation -Encoding UTF8 -Delimiter ";" -Path $csv_exportpath_mailboxplanretention ;

Get-MailboxPlan | Set-MailboxPlan -RetainDeletedItemsFor 30.00:00:00 ;
Get-Mailbox -ResultSize Unlimited | Where-Object {$_.UseDatabaseRetentionDefaults -eq $true} | Set-Mailbox -UseDatabaseRetentionDefaults $false ;
Get-Mailbox -ResultSize Unlimited | Set-Mailbox -RetainDeletedItemsFor 30 ;