#Address OneDrive performance issues on AVD by removing registry keys
$registry_path = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\SyncRootManager\"
$subkeys = Get-ChildItem -Path $registry_path

foreach ($key in $subkeys) {
    <# $key is $subkeys item #>
    Remove-Item -Path $key.PsPath -Recurse -Force
}
