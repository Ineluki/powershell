Connect-AzAccount ;

#Gather information and select hoostpool
$hostpool = Get-AzWVDHostPool | Select-Object Name,ID | Out-Gridview -Title "Select Hostpool" -OutputMode Multiple ;
$hostpool_name = $hostpool.Name ;
resourcegroup_name = $hostpool.Id.Split("/")[4] ;

#Gather information and select sessions to logoff
$sessions_overview = Get-AzWVDUserSession -HostPoolName $hostpool_name -ResourceGroupName $resourcegroup_name ;
$sessions_overview | Format-Table UserPrincipalName,SessionState,CreateTime ;

$session_choice = session_overview | Select-Object UserPrincipalName,SessionState,CreateTime,Name,Id | Out-Gridview -Title "Select sessions to logoff" -OutputMode Multiple ;
if ($null -eq $session_choice) {
    <# Action to perform if the condition is true #>
    Write-Host "No session were chosen! Existing script" -BackgroundColor White -ForegroundColor Magenta ;
} else {
    <# Action when all if and elseif conditions are false #>

    #Loop through selected sessions and log them all off
    $closedsessions = @() ;

    foreach ($session_chosen in $session_choice) {
        <# $session_chosen is the current item #>
        try {
            Remove-AzWVDUserSession -HostPoolName $hostpool_name -ResourceGroupName $resourcegroup_name -Id $session_chosen.Name.Split("/")[2] -SessionHostname $session_chosen.Name.Split("/")[1] ;
            $closedsessions += $session_chosen ;
        }
        catch {
            <#Do this if a terminating exception happens#>
            $error ;
            Get-AzWVDUserSession -HostPoolName $hostpool_name -ResourceGroupName $resourcegroup_name | Format-Table UserPrincipalName,SessionState,CreateTime ;
        }
    }
    $closedsessions | Format-Table ;
}
