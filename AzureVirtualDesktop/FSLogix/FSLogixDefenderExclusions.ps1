#Windows Defender Exclusions
#FSLogix documentation: https://learn.microsoft.com/en-us/fslogix/concepts-configuration-examples 
#Group Policy Templates: https://learn.microsoft.com/en-us/fslogix/how-to-use-group-policy-templates

#Process exclusions
%Programfiles%\FSLogix\Apps\frxccd.exe
%Programfiles%\FSLogix\Apps\frxccds.exe
%Programfiles%\FSLogix\Apps\frxsvc.exe

#Filepath exclusions
%Programfiles%\FSLogix\Apps\frxdrv.sys
%Programfiles%\FSLogix\Apps\frxdrvvt.sys
%Programfiles%\FSLogix\Apps\frxccd.sys
%TEMP%*.VHD
%TEMP%*.VHDX
%Windir%\TEMP*.VHD
%Windir%\TEMP*.VHDX
\\storageaccount.file.core.windows.net\share**.VHD
\\storageaccount.file.core.windows.net\share**.VHDX