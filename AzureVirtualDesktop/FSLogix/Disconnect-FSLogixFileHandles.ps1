#Provide basic information
$tenant = ""
$subscription = ""
$sharename = ""
$storageaccount = ""
$resourcegroup = ""
$profilepath = ""
$userprincipalname = ""

#Revoke all user sessions
Revoke-MgUserSignInSession -UserId $userprincipalname

#Connect to Azure service
Connect-AzAccount -Tenant $tenant -Subscription $subscription

#Update default Config if needed
#Update-AzConfig -DefaultSubscriptionForLogin $subscription

Set-AzCurrentStorageAccount -Name $storageaccount -ResourceGroupName $resourcegroup

Get-AzStorageFileHandle -ShareName $sharename -Recursive | Select-Object * | Out-GridView

Get-AzStorageFileHandle -ShareName $sharename -Recursive | Where-Object {$_.Path -like $profilepath} | Close-AzStorageFileHandle -Sharename $sharename

