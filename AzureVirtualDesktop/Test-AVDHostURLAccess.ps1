#https://learn.microsoft.com/en-us/azure/virtual-desktop/safe-url-list?tabs=azure
#https://learn.microsoft.com/en-us/azure/virtual-desktop/required-url-check-tool

$testresultsummary = @() ;

$urllist_port80 = @(
    "169.254.169.254"
    "168.63.129.16"
    "oneocsp.microsoft.com"
    "www.microsoft.com"
    "www.msftconnecttest.com"
)
$urllist_port443 = @(
    "login.microsoftonline.com"
    "rdbroker-g-us-r0.wvd.microsoft.com"
    "rdbroker-g-us-rl.wvd.microsoft.com"
    "rddiagnostics-g-us-r0.wvd.microsoft.com"
    "rddiagnostics-g-us-r1.wvd.microsoft.com"
    "rdweb-g-us-ro.wvd.microsoft.com"
    "rdweb-g-us-rl.wvd.microsoft.com"
    "rdgateway-g-us-r0.wvd.microsoft.com"
    "rdgateway-g-us-r1.wvd.microsoft.com"
    "catalogartifact.azureedge.net"
    "production.diagnostics.monitoring.core.windows.net"
    "wvdportalstorageblob.blob.core.windows.net"
    "maupdateaccount.blob.core.windows.net"
    "www.wvd.microsoft.com"
    "rdbroker.wvd.microsoft.com"
    "rddiagnostics.wvd.microsoft.com"
    "rdweb.wvd.microsoft.com"
    "rdgateway.wvd.microsoft.com"
    "rdbroker-r0.wvd.microsoft.com"
    "rdbroker-r1.wvd.microsoft.com"
    "rddiagnostics-r0.wvd.microsoft.com"
    "rddiagnostics-r1.wvd.microsoft.com"
    "rdweb-wvd.microsoft.com"
    "rdweb-r1.wvd.microsoft.com"
)
$urllist_port1688 = @(
    "kms.core.windows.net"
    "azkms.core.windows.net"
)

foreach ($url in $urllist_port80) {
    $test = Test-NetConnection -ComputerName $url -Port 80 ;
    $testresult_port80 = [PSCustomObject]@{
        'URL' = $test.ComputerName
        'RemoteAddress' = $test.RemoteAddress
        'RemotePort' = $test.RemotePort
        'InterfaceAlias' = $test.InterfaceAlias
        'SourceAddress' = $test.SourceAddress
        'TcpTestSucceeded' = $test.TcpTestSucceeded
        'Port' = 80
    }
    $testresultsummary += $testresult_port80 ;    
}

foreach ($url in $urllist_port443) {
    $test = Test-NetConnection -ComputerName $url -Port 443 ;
    $testresult_port443 = [PSCustomObject]@{
        'URL' = $test.ComputerName
        'RemoteAddress' = $test.RemoteAddress
        'RemotePort' = $test.RemotePort
        'InterfaceAlias' = $test.InterfaceAlias
        'SourceAddress' = $test.SourceAddress
        'TcpTestSucceeded' = $test.TcpTestSucceeded
        'Port' = 443
    }
    $testresultsummary += $testresult_port443 ;    
}

foreach ($url in $urllist_port1688) {
    $test = Test-NetConnection -ComputerName $url -Port 1688 ;
    $testresult_port1688 = [PSCustomObject]@{
        'URL' = $test.ComputerName
        'RemoteAddress' = $test.RemoteAddress
        'RemotePort' = $test.RemotePort
        'InterfaceAlias' = $test.InterfaceAlias
        'SourceAddress' = $test.SourceAddress
        'TcpTestSucceeded' = $test.TcpTestSucceeded
        'Port' = 1688
    }
    $testresultsummary += $testresult_port1688 ;    
}

$testresultsummary | Format-Table URL,TcpTestSucceeded,Port ;