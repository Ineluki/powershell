#Documentation: https://github.com/The-Virtual-Desktop-Team/Virtual-Desktop-Optimization-Tool
#Linking to releases: https://docs.github.com/en/repositories/releasing-projects-on-github/linking-to-releases
#Link to latest release: https://github.com/The-Virtual-Desktop-Team/Virtual-Desktop-Optimization-Tool/releases/latest


##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
}
#Create a timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

##Create a new folder
$newfolder_name = "VDOT" ;
$newfolder_name += $timestamp ;
$newfolder_path = "C:\Temp\" ;
$newfolder_path += $newfolder_name ;

if ((Test-Path -Path $newfolder_path) -ne $true) {
    New-Item -ItemType Directory -Path $newfolder_path ;   
}


$github_repository = "The-Virtual-Desktop-Team/Virtual-Desktop-Optimization-Tool"
$repository_releases_url = "https://api.github.com/repos/$github_repository/releases/latest"

$software_downloadlink = (Invoke-WebRequest -Uri $repository_releases_url | ConvertFrom-Json)[0].zipball_url

Invoke-WebRequest -Uri $software_downloadlink -Outfile "$newfolder_path\vdot.zip" 

$archive_folderpath = Expand-Archive -PassThru -Path "$newfolder_path\vdot.zip" -DestinationPath $newfolder_path
$archive_folderpath = $archive_folderpath[0].Name

$vdot_folderpath = "$newfolder_path\VirtualDesktopOptimizationTool"
Rename-Item -Path "$newfolder_path\$archive_folderpath" -NewName $vdot_folderpath

Set-Location -Path $vdot_folderpath ;
Unblock-File -Path .\VDOT_WhatIf.ps1 ;
Unblock-File -Path .\Windows_VDOT.ps1 ;

Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process ;
.\Windows_VDOT.ps1 -Optimizations All -Verbose -AcceptEula 

