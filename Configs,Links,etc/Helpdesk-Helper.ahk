#Requires AutoHotkey v2.0

;===============================================================================================================
;###Triggerkeys
;===============================================================================================================

;###Set triggerkeys to TAB and ENTER
#Hotstring EndChars `n`t

;===============================================================================================================
;###Scriptwide variables
;===============================================================================================================

;###Name of the scriptuser
global Name := ""

;===============================================================================================================
;###AHK Tools
;===============================================================================================================

;###Select date from popup menue and paste it
::dt::
{
CurrentDate := Gui()
CurrentDate.Add("MonthCal", "vMyDateTime")
ButtonOne := CurrentDate.AddButton("Default", "OK")
ButtonOne.OnEvent("Click",ButtonOneClicked) 
CurrentDate.Show()

ButtonOneClicked(Ctrl, *)
{
ChosenDate := Ctrl.Gui.Submit()
ChosenDate := String(ChosenDate)
}
}

;###Fast Search of marked text
^s::
{
clipboard := ""
Send "^c"
Send "^c"
Clipwait
Run 'https://www.startpage.com/sp/search?query=%clipboard%'
Return
}

;###Sends clipboard as key presses (for VmWare console e.g.)
^!v::
{
SendInput "{Raw}" A_Clipboard
}

;===============================================================================================================
;###Software, Paths and Tools
;===============================================================================================================

;###Sends URL for Feeddiscovery in "Remotedesktop" App
::avdfeed::
{
SendText "https://rdweb.wvvd.microsoft.com/api/arm/feeddiscovery"
}

;###Sends URL for AVD access via browser
::avdbrowser::
{
SendText "https://client.wvd.microsoft.com/arm/webclient"
}

;###Remotedesktop App Download Link
::avddownload::
{
SendText "https://learn.microsoft.com/en-us/azure/virtual-desktop/whats-new-client-windows"
}

;###Microsoft Installer Debugging Tool
::msinstalldebug::
{
SendText "https://download.microsoft.com/download/7/E/9/7E9188C0-2511-4B01-8B4E-0A641EC2F600/MicrosoftProgram_Install_and_Uninstall.meta.diagcab"
}

;###Microsoft Installer Debugging Tool
::msdotnetdebug::
{
SendText "https://www.microsoft.com/en-us/download/confirmation.aspx?id=30135"
}

;###Microsoft .NET 4.8.1 
::msdotnetdownload::
{
SendText "https://go.microsoft.com/fwlink/?LinkId=2203304"
}

;###Microsoft Email Header Analyzer
::msha::
{
SendText "https://mha.azurewebsites.net/pages/mha.html"
}

;###Intune Task Logpath auf Clients
::intuneclientlogpath::
{
SendText "%ProgramData%\Microsoft\IntuneManagementExtension\Logs"
}

;===============================================================================================================
;###Communication helpers
;===============================================================================================================

::mfg::
{
SendText "Mit freundlichen Grüßen{Enter}{Enter}"
SendInput %Name%
}

::mfge::
{
SendText "Best Regards{Enter}{Enter}"
SendInput %Name%
}

::telw::
{
SendText "Sehr geehrte Frau %Clipboard%,{Enter}{Enter}"
SendText "vielen Dank für unser freundliches Telefonat.{Enter}"
SendText "Nachfolgend möchte ich gerne zusammenfassen, was wir besprochen haben:{Enter}"
SendText "{Enter} {Enter} {Enter}"
}

::telm::
{
SendText "Sehr geehrter Herr %Clipboard%,{Enter}{Enter}"
SendText "vielen Dank für unser freundliches Telefonat.{Enter}"
SendText "Nachfolgend möchte ich gerne zusammenfassen, was wir besprochen haben:{Enter}"
SendText "{Enter} {Enter} {Enter}"
}

::tleiderw::
{
SendText "Sehr geehrte Frau %Clipboard%,{Enter}{Enter}"
SendText "leider konnte ich Sie telefonisch nicht erreichen."
}

::tleiderm::
{
SendText "Sehr geehrter Herr %Clipboard%,{Enter}{Enter}"
SendText "leider konnte ich Sie telefonisch nicht erreichen."
}

::tclosem::
{
SendText "Sehr geehrter Herr %Clipboard%,{Enter}{Enter}"
SendText "vielen Dank für Ihre Rückmeldung{!}{Enter}{Enter}"
SendText "Damit schliessen wir das aktuelle Ticket, aber selbstverständlich können Sie sich weiterhin dazu melden.{Enter}"
SendText "Sie unterstützen uns, wenn Sie bei Rückfragen stets eine Ticketnummer angeben.{Enter}{Enter}"
}

::tclosew::
{
SendText "Sehr geehrte Frau %Clipboard%,{Enter}{Enter}"
SendText "vielen Dank für Ihre Rückmeldung{!}{Enter}{Enter}"
SendText "Damit schliessen wir das aktuelle Ticket, aber selbstverständlich können Sie sich weiterhin dazu melden.{Enter}"
SendText "Sie unterstützen uns, wenn Sie bei Rückfragen stets eine Ticketnummer angeben.{Enter}{Enter}"
}

::tcloseohnem::
{
SendText "Sehr geehrter Herr %Clipboard%,{Enter}{Enter}"
SendText "bislang haben wir leider keine weitere Rückmeldung von Ihnen erhalten.{Enter}"
SendText "Wir gehen daher davon aus dass Sie mit der Bearbeitung zufrieden sind und schliessen das aktuelle Ticket.{Enter}{Enter}"
SendText "Selbstverständlich können Sie sich nach wie vor zum Ticket melden, Sie erreichen uns Werktags von 7:00 - 18:00 Uhr unter 08389 / 9200-801 oder einfach mit einer Antwort auf diese E-Mail.{Enter}"
}

::tcloseohnew::
{
SendText "Sehr geehrte Frau %Clipboard%,{Enter}{Enter}"
SendText "bislang haben wir leider keine weitere Rückmeldung von Ihnen erhalten.{Enter}"
SendText "Wir gehen daher davon aus dass Sie mit der Bearbeitung zufrieden sind und schliessen das aktuelle Ticket.{Enter}{Enter}"
SendText "Selbstverständlich können Sie sich nach wie vor zum Ticket melden, Sie erreichen uns Werktags von 7:00 - 18:00 Uhr unter 08389 / 9200-801 oder einfach mit einer Antwort auf diese E-Mail.{Enter}"
}

::ttv::
{
SendText "Wir würden uns die Thematik gerne mit Ihnen gemeinsam im Rahmen einer Fernwartungssitzung über Teamviewer ansehen.{Enter}"
SendText "Bitte teilen Sie uns dazu einen Zeitpunkt mit, an dem Sie an Ihrem PC mit aktiver Internetverbindung arbeiten und wir Sie telefonisch erreichen können.{Enter}{Enter}"
SendText "Vielen Dank{!}"
}

::tkat::
{
SendText "Ticketerstellung, Kategorisierung, Priorisierung" 
}

::msglobalincident::
{
    SendText "Betreff: Aktuelle Störungsmeldung - Microsoft Produkte betroffen{Enter}{Enter}"
    SendText "Sehr geehrte Damen und Herren,{Enter}{Enter}"
    SendText "aktuell sind die Microsoft Services (O365) von einer globalen Störung betroffen.{Enter}"
    SendText "Im Detail sind folgende Anwendungen und Dienste unter den Umständen teilweise oder vollständig gestört:{Enter}"
    SendText "Teams, OneDrive, Outlook (ExchangeOnline), SharePointOnline, Azure Virtual Desktop (Cloud Desktop){Enter}"
    SendText "Wir sind bereits mit dem Hersteller Microsoft in Kontakt und bitten Sie um Geduld bis zur Wiederherstellung Ihrer Dienste,{Enter}" 
    SendText "vielen Dank für Ihr Verständnis{!}{Enter}"
    SendText "Mit freundlichen Grüßen{Enter}{Enter}"  
    SendText "Ihr IT-ServiceDesk"   
}

::msglobalincidente::
{
    SendText "Subject: Incident notification - Microsoft services affected{Enter}{Enter}"
    SendText "Dear Sir or Madam,{Enter}{Enter}"
    SendText "we are currently facing global issues with Microsoft services.{Enter}"
    SendText "The following services are known to be affected:{Enter}"
    SendText "Teams, OneDrive, Outlook (ExchangeOnline), SharePointOnline, Azure Virtual Desktop (Cloud Desktop){Enter}"
    SendText "We are already in contact with Microsoft and kindly ask you for your patience.{Enter}" 
    SendText "Thank you in advance{!}{Enter}{Enter}"
    SendText "Kind regards{Enter}"
    SendText "IT Helpdesk"
}

;===============================================================================================================
;###Checklists
;===============================================================================================================

::officecheck::
{
    SendText "#Office-Troubleshooting#"
    SendText "###Nach jedem Schritt testen!###"
    SendText "#----------------------------------------------------------------------------------------------------------------------"
    SendText "[] Prüfen ob Benutzer/in lizensiert ist"
    SendText "[] Prüfen ob das Installationslimit von 5 Installationen erreicht ist"
    SendText "... https://admin.microsoft.com --> Active Users --> Select User --> Account --> Microsoft 365 Activations --> Show Activations"
    SendText "[] Office365-Cache leeren"
    SendText "… Powershell: Remove-Item -Recurse -Force -Path '$env:LOCALAPPDATA\Office\16.0\OfficeFileCache'"
    SendText "[] Office-Onlinereparatur durchführen"
    SendText "[] Zugangsdaten im 'Credential Manager' bzw. der 'Anmeldeinformationsverwaltung' löschen"
    SendText "...Powershell: Start-Process control.exe keymgr.dll" 
    SendText "[] Alle Sitzungen der Benutzer/in im Entra widerrufen"
    SendText "[] Rechner neu starten"
    SendText "#----------------------------------------------------------------------------------------------------------------------"
    SendText "Legende"
    SendText "[ ] -> Offener Punkt der Checkliste, noch zu erledigen"
    SendText "[x] -> erledigt, Maßnahme erfolgreich durchgeführt (Ergebnis dokumentieren{!})"
    SendText "[-] -> Durchführung nicht notwendig, Durchführung gescheitert bzw. ohne Ergebnis"
}

::teamscheck::
{
    SendText "#Check and repair 'New Teams'#"
    SendText "#----------------------------------------------------------------------------------------------------------------------"
    SendText "[ ]Check if Teams System requirements are fullfilled: https://learn.microsoft.com/en-us/microsoftteams/hardware-requirements-for-the-teams-app"
    SendText "[ ]Get Windows OS version, installed Teams version -> Post in Ticket"
    SendText "[ ]Run repair commands: DISM /Online /Cleanup-Image /RestoreHealth {;} SFC /scannow"
    SendText "[ ]Delete Teams Cache: Remove-Item -Recurse -Force -Path '$env:APPDATA\Microsoft\Teams'"
    SendText "[ ]Reset Teams App: Remove-Item -Recurse -Force -Path '$env:USERPROFILE\appdata\local\Packages\MSTeams_8wekyb3d8bbwe\LocalCache\Microsoft\MSTeams'"
    SendText "[ ]Check Windows Updates and Driver Updates, install if neccessary"
    SendText "[ ]Check Teams App settings : application needs to be allowed to run in background ALWAYS"
    SendText "[ ]Restart PC (only possible if installed on local device)"
    SendText "#----------------------------------------------------------------------------------------------------------------------"
    SendText "Explanation"
    SendText "[ ] -> Open task of the checklist, to be done"
    SendText "[x] -> DONE, Task completed successfully (don't forget documentation{!}"
    SendText "[-] -> Task unneccessary or could not be finished"
}

::outlookcheck::
{
    SendText "#Outlook Troubleshooting#"
    SendText "#----------------------------------------------------------------------------------------------------------------------"
    SendText "[] Prüfen ob das Problem auch in der OWA auftritt (falls nein -> reines Outlook Problem)"
    SendText "... Browser: https://outlook.office.com"
    SendText "[] Prüfen ob Benutzer Lizenz und Postfach besitzt"
    SendText "[] Outlook Version prüfen und dokumentieren"
    SendText "[] Outlook schliessen, Benutzer abmelden, im EntraID alle Sitzungen beenden, Neu anmelden"
    SendText "[] Outlook-Profil LOESCHEN (nicht nur ein neues Profil anlegen, sondern die Dateien aus dem Verzeichnis ENTFERNEN) und DANACH ein neues Profil anlegen"
    SendText "[] Outlook-Onlinereparatur durchführen (WICHTIG)"
    SendText "[] Windows Reparaturbefehle ausführen"
    SendText "... Powershell: DISM /Online /Cleanup-Image /RestoreHealth"
    SendText "... Powershell: SFC /scannow"
    SendText "[] Anmeldeinformationsverwaltung prüfen und dort gespeicherte Informationen die mit Office-Produkten in Zusammenhang stehen löschen"
    SendText "...Powershell: Start-Process control.exe keymgr.dll" 
    SendText "[] Rechner bzw. Server neu starten"
    SendText "... Powershell: Restart-Computer -ComputerName $env:computername"
    SendText "[] VPN-/Netzwerkverbindung prüfen"
    SendText "... Powershell: tracert heise.de" 
    SendText "[] DNS Auflösung prüfen, wird Adresse von Outlook korrekt aufgelöst?"
    SendText "... Powershell: nslookup autodiscover.outlook.com"
    SendText "[] Outlook im Safe-Modus starten (wenn Problem nicht erneut auftritt ist die Problemursache wahrscheinlich ein Add-In)"
    SendText "#----------------------------------------------------------------------------------------------------------------------"
    SendText "Legende"
    SendText "[ ] -> Offener Punkt der Checkliste, noch zu erledigen"
    SendText "[x] -> erledigt, Maßnahme erfolgreich durchgeführt (Ergebnis dokumentieren{!})"
    SendText "[-] -> Durchführung nicht notwendig, Durchführung gescheitert bzw. ohne Ergebnis"
}
