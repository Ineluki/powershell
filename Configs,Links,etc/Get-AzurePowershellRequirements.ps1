###Download and install PowerShell modules and connect to O365 services

##Set TLS version to 1.2 to avoid version mismatch: https://devblogs.microsoft.com/powershell/powershell-gallery-tls-support/
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 ;
##Set Powershell execution policy to remote signed
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Confirm:$false ;
##Allow PsGallery as repository
Set-PSRepository -Name "PsGallery" -InstallationPolicy Trusted ;
Register-PSRepository -Default ;
##Check currently installed modules, install missing modules in user scope if neccessary
#Azure Tenant administration
if ((Get-Module -ListAvailable -Name "Az.Accounts") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Az.Accounts
}
#ExchangeOnline module
if ((Get-Module -ListAvailable -Name "ExchangeOnlineManagement") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name ExchangeOnlineManagement
}
#MS Teams module
if ((Get-Module -ListAvailable -Name "MicrosoftTeams") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name MicrosoftTeams
}
#SharePointOnline module
if ((Get-Module -ListAvailable -Name "Microsoft.Online.SharePoint.PowerShell") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Microsoft.Online.SharePoint.PowerShell
}
#AzureVirtualDesktop module
if ((Get-Module -ListAvailable -Name "Microsoft.RDInfra.RDPowershell") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Microsoft.Online.SharePoint.PowerShell
}
if ((Get-Module -ListAvailable -Name "Az.DesktopVirtualization") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Az.DesktopVirtualization
}
if ((Get-Module -ListAvailable -Name "Az.Resources") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Az.Resources
}
if ((Get-Module -ListAvailable -Name "Az.Storage") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Az.Storage
}
#GraphAPI module
if ((Get-Module -ListAvailable -Name "Microsoft.Graph") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Microsoft.Graph
}
#SecretStore
if ((Get-Module -ListAvailable -Name "Microsoft.Powershell.SecretStore") -eq $false) {
    <# Action to perform if the condition is true #>
    Install-Module -Name Microsoft.Powershell.SecretStore
}
