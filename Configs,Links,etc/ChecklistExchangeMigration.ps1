#Checklist with comments

#Collect data of current config and save it (documentation)

#Run Healtchecker script
https://microsoft.github.io/CSS-Exchange/Diagnostics/HealthChecker/
https://gitlab.com/Ineluki/powershell/-/blob/master/Exchange/Run-HealthChecker.ps1 

#Collect additional data

#Exchange Server
<#
#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>
#Exchange receive connectors
Get-ReceiveConnector | Select-Object Identity,AuthMechanism,@{Name="Bindings";Expression={$_.Bindings -join ","}},@{Name="PermissionGroups";Expression={$_.PermissionGroups -join ","}},TlsCertificateName,Enabled,Server,TransportRole,ProtocolLoggingLevel,WhenChanged,WhenCreated,DomainSecureEnabled,MaxMessageSize,ConnectionTimeout,ConnectionInactivityTimeout,MessageRateLimit | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "C:\Temp\HealthcChecker\receiveconnectors.csv" ;

#Exchange send connectors
Get-SendConnector | Select-Object Identity,@{Name="AddressSpaces";Expression={$_.AddressSpaces -join ","}},SourceIPAddress,Port,@{Name="SmartHosts";Expression={$_.SmartHosts -join ","}},SmartHostsString,DNSRoutingEnabled,UseExternalDNSServersEnabled,TlsCertificateName,Enabled,ProtocolLoggingLevel,WhenChanged,WhenCreated,MaxMessageSize,ConnectionTimeout,ConnectionInactivityTimeout,MessageRateLimit,IsSMTPConnector | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "C:\Temp\HealthcChecker\sendconnectors.csv" ;

#Exchange Server Certificates
Get-ExchangeCertificate | Select-Object Identity,FriendlyName,Thumbprint,Status,Subject,Issuer,Services,@{Name="DNSNameList";Expression={$_.DNSNameList -join ","}},@{Name="CertificateDomains";Expression={$_.CertificateDomains -join ","}},NotBefore,NotAfter,PrivateKeyExportable,PublicKeySize,IsSelfSigned,IISServices | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "C:\Temp\HealthcChecker\exchangecertificates.csv" ;

#ActiveDirectory
#Import-Module ActiveDirectory
#Set-AdServerSettings -ViewEntireForest $true 

#Get Active Directory Domain Controller that hold FSMO roles

$dnm = (Get-ADForest).DomainNamingMaster ;
$sm = (Get-ADForest).SchemaMaster ;
$im = (Get-ADDomain).InfrastructureMaster ;
$pdc = (Get-ADDomain).PDCEmulator ;
$rid = (Get-ADDomain).RIDMaster ;

$fsmo = [PSCustomObject]@{
    'DomainNamingMaster' = $dnm
    'SchemaMaster' = $sm
    'InfrastructureMaster' = $im
    'PDCEmulator' = $pdc
    'RIDMaster' = $rid
}
$fsmo | Select-Object * | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "C:\Temp\HealthcChecker\fsmo.csv"