function Clear-MicrosoftTeamsCache {
    <#
    .SYNOPSIS
        Deletes cached files for Microsoft Teams, can also reset the app.
    .DESCRIPTION
        Clear-MicrosoftTeamsCache is a function that will delete files from the appdata folder in the current users' profile.
    .EXAMPLE
        Clear-MicrosoftTeamsCache ;
    .INPUTS
        none
    .OUTPUTS
        Console
    .NOTES
    
    Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute
    Author: Ineluki
    Message: GUI fails, Powershell prevails!
    #>
    
    [CmdletBinding()]
    param (
        #Reset app settings
        [Parameter(Mandatory=$false,ParameterSetName='ResetApp',HelpMessage='Resets the app itself, deleting all custom settings')]
        [switch]
        $ResetApp
    )
    
    if (Test-Path -Path "$env:APPDATA\Microsoft\Teams") {
        <# Action to perform if the condition is true #>
        Remove-Item -Recurse -Force -Path "$env:APPDATA\Microsoft\Teams"
    } else {
        <# Action when all if and elseif conditions are false #>
        Write-Host "Teams Cache folder can not be found!" -ForegroundColor White -BackgroundColor Magenta
    }

    if ($ResetApp.IsPresent) {
        <# Action to perform if the condition is true #>
        if (Test-Path -Path "$env:USERPROFILE\appdata\local\Packages\MSTeams_8wekyb3d8bbwe\LocalCache\Microsoft\MSTeams") {
            <# Action to perform if the condition is true #>
            Stop-Process -Confirm:$false -Force -Name "ms-teams" 
            Remove-Item -Recurse -Force -Path "$env:USERPROFILE\appdata\local\Packages\MSTeams_8wekyb3d8bbwe\LocalCache\Microsoft\MSTeams"
        } else {
            <# Action when all if and elseif conditions are false #>
            Write-Host "Teams Appdata folder can not be found!" -ForegroundColor White -BackgroundColor Magenta
        }
    }
    
}