
#Define variables for later usage
$url = "https://startpage.com" ;
$shortcutpath = [Environment]::GetFolderPath("Desktop") ;
$shortcutpath += "\startpage.url" ;

#Create a new empty file
New-Item -Path $shortcutpath -ItemType File ;
#Fill the file with content
Add-Content -Path $shortcutpath -Value "[DEFAULT]" ;
Add-Content -Path $shortcutpath -Value "BASEURL=$url" ;
Add-Content -Path $shortcutpath -Value "[InternetShortcut]" ;
Add-Content -Path $shortcutpath -Value "URL=$url" ;

