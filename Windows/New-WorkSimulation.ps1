# Define a function to get installed programs from the registry
function Get-InstalledPrograms {
    $uninstallKey = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall"
    $subKeys = Get-ChildItem $uninstallKey
    foreach ($subKey in $subKeys) {
        $programName = (Get-ItemProperty $subKey.PSPath).DisplayName
        if ($programName) {
            $programName
        }
    }
}

# Define an array of important system processes to exclude
$system32Processes = Get-Process | Where-Object { $_.Path -like "C:\Windows\System32\*" } | Select-Object -Property Path -Unique ;

$system32ProcessesExecutables = @() ;
$system32Processes | ForEach-Object {
    $exeName = Split-Path $_.Path -Leaf
    $system32ProcessesExecutables += [PSCustomObject]@{
        'Executable' = $exeName
    }
}
$system32ProcessesExecutables = ($system32ProcessesExecutables).Executable ;
$system32ProcessesExecutables;


# Infinite loop to randomly start and close installed programs
while ($true) {
    # Choose a random program from the installed programs list
    $randomProgram = Get-Random -InputObject (Get-InstalledPrograms)

    # Start the chosen program
    Start-Process -FilePath $randomProgram

    # Wait for a random period of time between 5 and 275 seconds before closing the program
    Start-Sleep -Seconds (Get-Random -Minimum 5 -Maximum 275)

    # Close the program unless it's an important system process
    if (-not $system32Processess.Contains((Split-Path -Leaf $randomProgram))) {
        Get-Process | Where-Object {$_.Name -eq (Split-Path -Leaf $randomProgram)} | Stop-Process -Force
    }
} 