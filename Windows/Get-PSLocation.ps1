function Get-PSLocation {
    <#
    .SYNOPSIS
        Return path of the currently running script which is executed in powershell.

    .DESCRIPTION
        Get-PSLocation is a function that returns the path of the script currently executed.
        The function takes different PowerShell versions into consideration.

    .EXAMPLE
        $myinvocation = Get-PSLocation ;

    .INPUTS
        none

    .OUTPUTS
        none

    .NOTES
        Important: Only returns a value when excuted as part of a script.
        Author: Ineluki
        Message: GUI fails, Powershell prevails!
    #>

    $psversion_major = $PSVersionTable.PSVersion.Major ;

    if ($null -eq $psversion_major) {
        $psversion_major = 1 ;
    } elseif ($psversion_major -le 2) {
        $location = Split-Path $MyInvocation.MyCommand.Path ;
    } else {
        $location = $PSScriptRoot.ToString() ;
    }
    Return $location ;
}