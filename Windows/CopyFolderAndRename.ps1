#Provide the absolute path to source and destination folders
$sourcefolderpath = "C:\...." ;
$destinationfolderpath = "C:\....." ;

#Provide a new name for the folder after it is being processed
$newfoldername = "" ;

#Test if provided paths do exist
try {
    Test-Path $sourcefolderpath ;
}
catch {
    <#Do this if a terminating exception happens#>
    Write-Host "" ;
    Write-Host "SOURCE folder does NOT exist!" -ForegroundColor Magenta -BackgroundColor White ;
    Write-Host "" ;
}

try {
    Test-Path $destinationfolderpath ;
}
catch {
    <#Do this if a terminating exception happens#>
    Write-Host "" ;
    Write-Host "DESTINATION folder does NOT exist!" -ForegroundColor Magenta -BackgroundColor White ;
    Write-Host "" ;
}

#List content of sourcefolder and save to variable
$sourcefoldercontent = Get-ChildItem -Path $sourcefolderpath ;

#Process the content of sourcefolder via pipeline and copy it to destinationfolder
$sourcefoldercontent | Copy-Item -Destination $destinationfolderpath ;

#Rename source folder
Rename-Item -Path $sourcefolderpath -NewName $newfoldername ;


