function Clear-WindowsUpdateCache {
    <#
    .SYNOPSIS
        Restart the relevant services and deletes the folder containing already downloaded files for windows update.
    .DESCRIPTION
        Clear-WindowsUpdateCache is a function that resets Windows Update in order to fix issues.
    .EXAMPLE
        Clear-WindowsUpdateCache ;
    .INPUTS
        none  
    .OUTPUTS
        none
    .NOTES
    
    Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute
    Author: Ineluki
    Message: GUI fails, Powershell prevails!
    #>

    <#
    #Possible next steps to repair Windows Update
    dism /online /cleanup-image /restorehealth ;
    sfc /scannow ;
    #>

    #Generate the absolute path for files
    ##Check if C:\Temp exists, create it otherwise
    if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
    }

    #Create timestamp
    $timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

    #Create filepath
    $services_currentstate = "C:\Temp\services_currentstate" ;
    $services_currentstate += "_" ;
    $services_currentstate += $timestamp ;
    $services_currentstate += ".csv" ;

    #Save current state of all services to CSV file
    Get-Service | Select-Object * | Export-Csv -Encoding UTF8 -NoTypeInformation -Path $services_currentstate ;

    #Check relevant services with console output
    Get-Service -Name "bits" | Format-List * ;
    Get-Service -Name "wuauserv" | Format-List * ;

    #Stop related services
    Stop-Service -Name "bits" ;
    Stop-Service -Name "wuauserv" ;
    Stop-Service -Name "cryptSvc" ;
    Stop-Service -Name "msiserver" ;

    #Clear the folder containing downloaded update files
    if (Test-Path -Path "$env:windir\softwaredistribution") {

        Remove-Item -Path "$env:windir\softwaredistribution\" -Recurse -Force ;
        Remove-Item -Path "$env:windir\System32\catroot2" -Recurse -Force ;

    } else {

        Write-Host "Error: couldn't find path: $winupdatefolderpath !" -ForegroundColor Magenta -BackgroundColor White ;

    }
    
    #Restart stopped services
    Start-Service -Name "bits" ;
    Start-Service -Name "wuauserv" ;
    Start-Service -Name "cryptSvc" ;
    Start-Service -Name "msiserver" ;  
}




