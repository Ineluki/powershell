function Add-SupportSoftware {

<#
.SYNOPSIS
    Creates an PsCustomObject, containing downloadlink and name of a software.

.DESCRIPTION
    Add-SupportSoftware is a function that creates an object containing direct download link and name.

.PARAMETER SupportSoftwareDownloadLink
    The direct download link (same as in the browser).

.PARAMETER SupportSoftwareName
    Specifies a Name, which is important for naming the file which will be downloaded.

.EXAMPLE
    Add-SupportSoftware -SupportSoftwareName "AdwCleaner" -SupportSoftwareDownloadLink "https://downloads.malwarebytes.com/file/adwcleaner"

.EXAMPLE
    Add-SupportSoftware "MicrosoftMSertTool(64-bit)" "https://go.microsoft.com/fwlink/?LinkId=212732"

.INPUTS
    String

.OUTPUTS
    PSCustomObject

.NOTES
    Author: Ineluki
    Message: GUI fails, Powershell prevails!
#>

    [CmdletBinding()]
    param (
        #Name of the software
        [Parameter(Mandatory=$true,Position=0)]
        [string]
        $SupportSoftwareName,
        #Direct download link
        [Parameter(Mandatory=$true,Position=1)]
        [string]
        $SupportSoftwareDownloadLink
    )
	
    $SupportSoftware = [PSCustomObject]@{
    'SoftwareName' = $SupportSoftwareName
    'DownloadLink' = $SupportSoftwareDownloadLink
    }

    return $SupportSoftware
}

#Adds and downloads relevant software

$softwarecollection = @();
$softwarecollection += Add-SupportSoftware -SupportSoftwareName "AdwCleaner" -SupportSoftwareDownloadLink "https://downloads.malwarebytes.com/file/adwcleaner" ;
$softwarecollection += Add-SupportSoftware -SupportSoftwareName "MicrosoftMSertTool(64-bit)" -SupportSoftwareDownloadLink "https://go.microsoft.com/fwlink/?LinkId=212732" ;
$softwarecollection += Add-SupportSoftware -SupportSoftwareName "IISCrypto.exe" -SupportSoftwareDownloadLink"https://www.nartac.com/Downloads/IISCrypto/IISCrypto.exe" ;

foreach ($software in $softwarecollection) {

    $toolname = $software.SoftwareName ;
    $tooldownloadlink = $software.DownloadLink  ;
    Start-BitsTransfer -Source $tooldownloadlink -Destination "C:\Temp\$toolname.exe" -DisplayName 'Get-SupportSoftware' ;  
}