function Remove-ScreenLock {
    
    <#
    .SYNOPSIS
        Starts a loop of simulated keystrokes to prevent screensaver/screenlock.

    .DESCRIPTION
        Remove-ScreenLock is a function that simulates pressing of "SCROLLLOCK" at randomized intervals between 1 and 240 seconds.

    .EXAMPLE
        Remove-Screenlock -InfiniteLoopingEnabled ;

    .EXAMPLE
        Remove-Screenlock -TimerInMinutes 30 ; 

    .INPUTS
        none

    .OUTPUTS
        none

    .NOTES
        Important: Don't use this at a location were other people have direct access to your PC!
        Author: Ineluki
        Message: GUI fails, Powershell prevails!
    #>
    
    [CmdletBinding(ConfirmImpact = "Low")]
    param (
        [Parameter(Mandatory=$true,ParameterSetName="InfiniteLoopingEnabled",HelpMessage="Runs the script infinitly until canceled.")]
        [switch]$InfiniteLoopingEnabled ,

        [Parameter(Mandatory=$false,ParameterSetName="TimerInMinutes",HelpMessage="Runs for a specific timespan in minutes.")]
        [int]$TimerInMinutes
    )
    
    #Creating new shell object
    $wscriptshell = New-Object -ComObject WScript.Shell ;

    if ($InfiniteLoopingEnabled.IsPresent) {
    #Loop to simulate keystrokes forever
    do {
        #Two differently timed keystrokes to avoid advanced idle detection
        $wscriptshell.SendKeys("{SCROLLLOCK}") ;
        #Using the random function to avoid regular intervals
        Start-Sleep -Milliseconds (Get-Random -Minimum 100 -Maximum 300) ;
        $wscriptshell.SendKeys("{SCROLLLOCK}") ;
        Start-Sleep -Seconds (Get-Random -Minimum 1 -Maximum 240) ;
    } while ($true)   

    } elseif ($TimerInMinutes) {
        #Save the start time for later reference
        $starttime = Get-Date ;
        #Loop to simulate keystrokes for the given numer of minutes
        do {
            #Two differently timed keystrokes to avoid advanced idle detection
            $wscriptshell.SendKeys("{SCROLLLOCK}") ;
            #Using the random function to avoid regular intervals
            Start-Sleep -Milliseconds (Get-Random -Minimum 100 -Maximum 300) ;
            $wscriptshell.SendKeys("{SCROLLLOCK}") ;
            Start-Sleep -Seconds (Get-Random -Minimum 1 -Maximum 240) ;
        } until (((Get-Date) -ge ($StartTime.AddMinutes($TimerInMinutes))))
    }
}


