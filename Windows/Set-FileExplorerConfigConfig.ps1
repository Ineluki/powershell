###
#File Explorer
###

#Set option to show hidden files
Set-ItemProperty -Path "$USERKEYPATH\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "Hidden" -Value '1' ;

#Set option to show file extensions
Set-ItemProperty -Path "$USERKEYPATH\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "HideFileExt" -Value '0' ;

