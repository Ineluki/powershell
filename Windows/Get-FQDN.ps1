function Get-FQDN {

    $fqdn = $env:COMPUTERNAME ;
    $fqdn += "." ;
    $fqdn += $env:userdnsdomain ;
    Write-Host $fqdn -ForegroundColor Magenta -BackgroundColor White ;
    
}