#This script moves files between fileshare locations

#Set variables for later use
##Import absolute filepaths from CSV 
$csv = Import-Csv -Path ".\files_to_move.csv" ;
##Variable for target folder path of the moving process
$file_target_path = "K:\" ;
##Path for various reports
$results = "C:\Temp\results.txt" ;
$errors_copy = "C:\Temp\copyerrors.txt" ;
$errors_deletion = "C:\Temp\deletionerrors.txt" ;

#Create various files to be able to fill it with data later on
New-Item -ItemType File -Path $results ;
New-Item -ItemType File -Path $errors_copy ;
New-Item -ItemType File -Path $errors_deletion ;

#Process the content of the CSV file
foreach ($file in $csv) {
    
    #Get year number out of folder name
    $string_split = $file.Split("-", 2) ;
    $string_split = $file.Split[0].Split("E", 2) ;
    $year = $string_split[1] ;

    #Create the correct target path
    switch ($year) {
        10 { $file_target_path += "2010\" ; break }
        11 { $file_target_path += "2011\" ; break }
        12 { $file_target_path += "2012\" ; break }
        13 { $file_target_path += "2013\" ; break }
        14 { $file_target_path += "2014\" ; break }
        15 { $file_target_path += "2015\" ; break }
        16 { $file_target_path += "2016\" ; break }
        17 { $file_target_path += "2017\" ; break }
        18 { $file_target_path += "2018\" ; break }
        19 { $file_target_path += "2019\" ; break }
        20 { $file_target_path += "2020\" ; break }
        21 { $file_target_path += "2021\" ; break }
        Default {Write-Host "Year for target path could not be extracted !" -ForegroundColor Magenta -BackgroundColor White ;}
    }

    #Generate the final absolute target path
    $file_target_path += $file ;

    #Test if file already exists at the target location
    if (Test-Path $file_target_path) {
        Write-Host "File $file_target_path already exists in target location ! Check if it is deleted in source directory !" -ForegroundColor Green -BackgroundColor White ;
        Add-Content -Path $errors_copy -Value $file_target_path ; 
    }
    else {
            try {
                #Copy command to process the files from the CSV files
                Copy-Item -Path $file -Destination $file_target_path -ErrorAction Stop ;
                Write-Host "Copy operation successfull ! $file is copied to target location now." ;
                Add-Content -Path $results -Value $file ;

                try {
                    #Remove the file after it was copied
                    Remove-Item -Path $file -Force -ErrorAction Stop ;
                    Write-Host "Source file $file was removed successfully !" -ForegroundColor Yellow -BackgroundColor Black ;
                }
                catch {
                    #Generate a small report when the remove command fails for any reason
                    Write-Host "REMOVAL of $file FAILED !" -ForegroundColor Red -BackgroundColor White ;
                    Write-Host "" ;
                    Write-Host "=========================================================================" -ForegroundColor Red ;
                    Write-Host "" ;
                    Add-Content -Path $errors_copy -Value "=====================================================";
                    Add-Content -Path $errors_copy -Value $file ;
                    Add-Content -Path $errors_copy -Value ("Errormessage: " + $_.ToString()) ;
                    Add-Content -Path $errors_copy -Value ("InvocationInfo.PositionMessage: " + $_.InvocationInfo.PositionMessage) ;
                    Add-Content -Path $errors_copy -Value ("InvocationInfo.PSCommandPath: " + $_.InvocationInfo.PSCommandPath) ;
                    Add-Content -Path $errors_copy -Value "=====================================================";
                    Write-Host "" ;
                    Write-Host "=========================================================================" -ForegroundColor Red ;
                    Write-Host "" ;
                }
            }
            catch {
                #Generate a small report when the copy command fails for any reason
                Write-Host "COPY of $file FAILED" -ForegroundColor Red -BackgroundColor White ;
                Write-Host "" ;
                Write-Host "=========================================================================" -ForegroundColor Red ;
                Write-Host "" ;
                Add-Content -Path $errors_copy -Value "=====================================================";
                Add-Content -Path $errors_copy -Value $file ;
                Add-Content -Path $errors_copy -Value ("Errormessage: " + $_.ToString()) ;
                Add-Content -Path $errors_copy -Value ("InvocationInfo.PositionMessage: " + $_.InvocationInfo.PositionMessage) ;
                Add-Content -Path $errors_copy -Value ("InvocationInfo.PSCommandPath: " + $_.InvocationInfo.PSCommandPath) ;
                Add-Content -Path $errors_copy -Value "=====================================================";
                Write-Host "" ;
                Write-Host "=========================================================================" -ForegroundColor Red ;
                Write-Host "" ;
            }
    }

}



