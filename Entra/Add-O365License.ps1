#Connect to M365/O365 tenant services
$tenant_credentials = Get-Credential ;
Connect-MsolService -Credential $tenant_credentials ;
Connect-AzureAD -Credential $tenant_credentials ;

#Read in the users who will be assigned new licenses
#File needs to be a simple TXT file with one user per line
$filepath = "" ;
$users = Get-Content -Path $filepath ;

#Process the user list and assign the new license
foreach ($user in $users) {
  
    $azure_user = Get-AzureADUser -SearchString $user ;
    $userUPN = ($azure_user.UserPrincipalName) ;
    if ($azure_user.UsageLocation) {

        #Set variables for the licenses
        $subscriptionFrom = "TEAMS_EXPLORATORY" ;
        $subscriptionTo = "TEAMS_COMMERCIAL_TRIAL" ;
        
        #Assign new license first to avoid disruptions
        $license = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicense ;
        $license.SkuId = (Get-AzureADSubscribedSku | Where-Object -Property SkuPartNumber -Value $subscriptionTo -EQ).SkuID ;
        $licenses = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses ;
        $licenses.AddLicenses = $License ;
        Set-AzureADUserLicense -ObjectId $userUPN -AssignedLicenses $licenses ;

        #Unassign old license
        $licenses = New-Object -TypeName Microsoft.Open.AzureAD.Model.AssignedLicenses ;
        $licenses.RemoveLicenses =  (Get-AzureADSubscribedSku | Where-Object -Property SkuPartNumber -Value $subscriptionFrom -EQ).SkuID ;
        Set-AzureADUserLicense -ObjectId $userUPN -AssignedLicenses $licenses ;

    } else {
       
        Write-Host "User $azure_user needs to have a usage location assigned!" -ForegroundColor White -BackgroundColor Red ;
        Write-Host "Please assign a location first and the re-run the script for user!" -ForegroundColor White -BackgroundColor Magenta ;

    }
}
