function New-KerberosKeyRollover {
    <#
    .SYNOPSIS
        Recreates and synchronizes the kerberos key for the 'AZUREADSSO' computer account, required for Seamless Single Sign-On feature.
    
    .DESCRIPTION
        Needs to be executed with an elevated Powershell on a Windows system with Azure AD Connect installed and configured !
        As described in the Microsoft Docs, the key should be renewed every 30 days.
        Microsoft Docs: https://docs.microsoft.com/en-us/azure/active-directory/hybrid/how-to-connect-sso-faq#how-can-i-roll-over-the-kerberos-decryption-key-of-the--azureadsso--computer-account-
    
    .EXAMPLE
        New-KerberosKeyRollover ;

    .INPUTS
        none

    .OUTPUTS
        none

    .NOTES
        Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute
        Author: Ineluki
        Message: GUI fails, Powershell prevails!
    #>
    
    try {
        #Set location to Azure AD Connect installation folder
        Set-Location $env:programfiles"\Microsoft Azure Active Directory Connect" ;
        
        #Import the SSO module for Powershell
        Import-Module .\AzureADSSO.psd1 ;

        #Create new authentication context/kerberos key - will require Global Administrator credentials
        New-AzureADSSOAuthenticationContext ;

        #Provides a list of AD forests on which SSO has been enabled
        Get-AzureADSSOStatus | ConvertFrom-Json ;

        #Get Domain Administrator credentials for the synchronization
        #Input EXAMPLE: contose.com\johndoe
        $domain_admin_creds = Get-Credential -Message "Domain Admin required! EXAMPLE: contoso.com\johndoe" ;

        #Synchronize the changes und update the kerberos key for the 'AZUREADSSO' computer account
        Update-AzureADSSOForest -OnPremCredentials $domain_admin_creds ;
    }
    catch {
        Write-Host "An issue occured and no changes have been made." -ForegroundColor Magenta -BackgroundColor White ;
        Write-Host "Please check the installation path, access rights and the provided credentials!" -ForegroundColor Magenta -BackgroundColor White ;
    }
}