#Parameters whose actual values be defined at the PRTG sensor itself ("Parameter" section in the sensor settings) and provided to the custom script
param($secretvalue,$appid,$tenantid)

#Prerequisites
#Powershell 7.2 or higher is the recommended version for use with Az module on all platforms
#Powershell Module "Az" required
#https://learn.microsoft.com/en-us/powershell/azure/install-azps-windows?view=azps-11.5.0&tabs=powershell&pivots=windows-psgallery

if (Get-InstalledModule -Name Az) {
    <# Action to perform if the condition is true #>
    Import-Module -Name Az ;
} else {
    <# Action when all if and elseif conditions are false #>
    Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser ;
    Install-Module -Name Az Repository PSGallery -Force ;
}

#Add credentials to powershell functions: https://learn.microsoft.com/en-us/powershell/scripting/learn/deep-dives/add-credentials-to-powershell-functions?view=powershell-7.4
function New-PsCredentials {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,ParameterSetName='Username',HelpMessage='Provide the username or something like an application id')]
        [string]
        $Username,
        [Parameter(Mandatory=$true,ParameterSetName='Password',HelpMessage='Provide the password or secret')]
        [string]
        $password
    )
    $password_securestring = ConvertTo-SecureString -String $password -AsPlainText -Force ;
    New-Object System.Management.Automation.PSCredential($username,$password_securestring) ;    
}
$pscredentials = New-PsCredentials -Username $appid -Password $secretvalue ;
function Check-AzureBackup {

    [CmdletBinding()]
    param ()
    

}