#Create a new local ActiveDirectory User Account

#Use parameters passed by PowerAutomate Workflow
param (
    [Parameter()]
    [string]
    $name_givenname,
    [Parameter()]
    [string]
    $name_surname,
    [Parameter()]
    [string]
    $manager,
    [Parameter()]
    [string]
    $title,
    [Parameter()]
    [string]
    $start,
    [Parameter()]
    [string]
    $department,
    [Parameter()]
    [string]
    $officephone,
    [Parameter()]
    [string]
    $mobilephone
)

##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
}
    
#Create timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

#Create a new local ActiveDirectory User Account
####
#Use parameters passed by PowerAutomate Workflow
param (
    [string]
    $name_givenname,
    [string]
    $name_surname,
    [string]
    $skillset,
    [string]
    $position,
    [string]
    $startdate,
    [string]
    $telefon,
    [string]
    $mobile,
    [string]
    $manager,
    [string]
    $emailaddress,
    [string]
    $worklocation
)

##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
} else {
    <# Action when all if and elseif conditions are false #>
    Write-Host "Creation and validation of 'C:\Temp' failed!"
}
#Create timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;
#Generate log file name
$logfile = "C:\Temp\runbook_logfile_" ; 
$logfile += $timestamp ;
$logfile += ".csv" ;
try {
    #Create the empty file
    New-Item -ItemType File -Path $logfile
    Write-Host "Logfile in 'C:\Temp' was successfully created!"
}
catch {
    <#Do this if a terminating exception happens#>
    Write-Host "Creation of local logfile failed!"
    throw $_.Exception
}

#Add all set parameters to logfile
Add-Content -Path $logfile -Value "-----------------------------------"
Add-Content -Path $logfile -Value "Uebergebene Parameter vom Workflow:"
Add-Content -Path $logfile -Value "Vorname: $name_givenname"


#Import Powershell module for local Active Directory commands
#This needs to beinstalled on the Hybrid Worker: Install-WindowsFeature RSAT-AD-Powershell -IncludeAllSubFeature ;
Import-Module ActiveDirectory ;

#Define CanonicalName (CN) path for user object
switch ($worklocation) {
    Munich {$user_cnpath =  "AAA"}
    London {$user_cnpath =  "BBB"}
    Default {$user_cnpath =  "CCC"}
}

#Definition of the UserPrincipalName (UPN)
$emailaddress.Trim() ;
$upn = $emailaddress ;
Add-Content -Path $logfile -Value "UserPrincipalName: $upn" 

#Definition of the sam account name
$samaccountname = $emailaddress.Split('@',2)[0] 

#List of forbidden characters for samaccountname
#https://learn.microsoft.com/en-us/windows/win32/adschema/a-samaccountname
$forbiddenCharacters = "/\[]:;|=,+*?<>"

#Function to check for forbidden characters
function Find-ForbiddenCharacters($Str, $ForbiddenChars) {
    foreach ($char in $forbiddenChars.ToCharArray()) {
        if ($str.Contains($char)) {
            return $true
        }
    }
    return $false
}

#Trim names
$name_givenname = $name_givenname.Trim() 
$name_surname = $name_surname.Trim()

#Check the length of Displayname and truncate it
$displayname = "$name_givenname $name_surname"
try {
    if ($displayname.Length -gt 18) {
        $displayname = $displayname.Substring(0, 18)
        Add-Content -Path $logfile -Value "Trimmed Displayname successfully!"
    }
}
catch {
    <#Do this if a terminating exception happens#>
    Add-Content -Path $logfile -Value "Trimming of Displayname failed!"
    throw $_
}

#Check the length of Name-Attribute and truncate it
$name = "$name_givenname $name_surname"
try {
    if ($name.Length -gt 18) {
        $name = $name.Substring(0, 18)
        Add-Content -Path $logfile -Value "Trimmed Name-Attribute successfully!"
    }
}
catch {
    <#Do this if a terminating exception happens#>
    Add-Content -Path $logfile -Value "Trimming of Name-Attribute failed!"
    throw $_
}

#Check if the string contains forbidden characters
if (Find-ForbiddenCharacters -Str $samaccountname -ForbiddenChars $forbiddenCharacters) {
    Add-Content -Path $logfile -Value "Samaccountname enthält ungültige Zeichen."
} else {
    #Check the length and truncate if necessary - maximum of 18 characters because of the "." in the sam account name generated before
    if ($samaccountname.Length -gt 18) {
        $samaccountname = $samaccountname.Substring(0, 18)
        Add-Content -Path $logfile -Value "Samaccountname enthält mehr als 15 Zeichen und wurde deshalb gekürzt."
    }
}
Add-Content -Path $logfile -Value "SamAccountName: $samaccountname"


#Define parameters for user account
$userparams = @{
 
    GivenName = $name_givenname
    Surname = $name_surname
    Displayname = "$name_givenname $name_surname"
    Name = "$name_givenname $name_surname"
    UserPrincipalname = "$name_givenname.$name_surname@myself.de"
    EmailAddress = "$name_givenname.$name_surname@myself.de"
    Path =
    City =
    Department = 
    Company =
    OfficePhone = 
    MobilePhone = 
    Title = 
    Manager = 
    StreetAddress = 
    PostalCode = 
    Fax = 
    Enabled = $false
    AccountPassword = (ConvertTo-SecureString -AsPlainText -Force -String "Welcome@Company!")
    PasswordNeverExpires = $true

}

New-AdUser @userparams ;

