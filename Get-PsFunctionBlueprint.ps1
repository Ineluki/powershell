function Get-PSFunctionBlueprint {
    
    [CmdletBinding(SupportsShouldProcess,ConfirmImpact = "Low")]
    param (
        [Parameter(Mandatory=$false,ParameterSetName="ExportToTxt",HelpMessage="Exports the blueprint structure into a TXT file")]
        [switch]$ExportToTxt ,

        [Parameter(Mandatory=$false,ParameterSetName="ExportToHTML",HelpMessage="Exports the blueprint structure to HTML file")]
        [switch]$ExportToHTML    
    )

        if ($ExportToTxt.IsPresent) {
            $filepath = ".\psfunctionblueprint.txt" ;
            Add-Content -Path $filepath -Value "function [Name] {" ;
            Add-Content -Path $filepath -Value "<#" ;
            Add-Content -Path $filepath -Value ".SYNOPSIS" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value ".DESCRIPTION" ;
            Add-Content -Path $filepath -Value "" ; ;
            Add-Content -Path $filepath -Value ".EXAMPLE" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value ".INPUTS" ; 
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value ".OUTPUTS" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value ".NOTES" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value "Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute" ;
            Add-Content -Path $filepath -Value "Author: Ineluki" ;
            Add-Content -Path $filepath -Value "Message: GUI fails, Powershell prevails!" ;
            Add-Content -Path $filepath -Value "#>" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value "[CmdletBinding(SupportsShouldProcess, ConfirmImpact = `"Low`")]" ;
            Add-Content -Path $filepath -Value "param (" ;
            Add-Content -Path $filepath -Value "[Parameter(Mandatory=`$true,Position=0,ParameterSetName='ParameterName01',HelpMessage='Helpmessage01')]" ;
            Add-Content -Path $filepath -Value "[string]" ;
            Add-Content -Path $filepath -Value "`$parameter01" ;
            Add-Content -Path $filepath -Value "," ;
            Add-Content -Path $filepath -Value " [Parameter(Mandatory=`$false,ParameterSetName='ParameterName02',HelpMessage='Helpmessage02')]" ;
            Add-Content -Path $filepath -Value "[switch]" ;
            Add-Content -Path $filepath -Value "`$parameter02" ;
            Add-Content -Path $filepath -Value ")" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value "" ;
            Add-Content -Path $filepath -Value "}" ;
        } elseif ($ExportToHTML.IsPresent) {
            $filepath = ".\psfunctionblueprint.html" ;
            Add-Content -Path $filepath -Value "<!DOCTYPE html>"
            Add-Content -Path $filepath -Value "<html>"
            Add-Content -Path $filepath -Value "<head>"
            Add-Content -Path $filepath -Value "<title>PsFunctionBlueprint</title>"
            Add-Content -Path $filepath -Value "</head>"
            Add-Content -Path $filepath -Value "<body>"
            Add-Content -Path $filepath -Value "<br>"
            Add-Content -Path $filepath -Value "<p> <br>"
            Add-Content -Path $filepath -Value "function [Name] { <br>"
            Add-Content -Path $filepath -Value "<# <br>" ;
            Add-Content -Path $filepath -Value ".SYNOPSIS <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value ".DESCRIPTION <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value ".EXAMPLE <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value ".INPUTS <br>" ; 
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value ".OUTPUTS <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value ".NOTES <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value "Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute <br>" ;
            Add-Content -Path $filepath -Value "Author: Ineluki <br>" ;
            Add-Content -Path $filepath -Value "Message: GUI fails, Powershell prevails! <br>" ;
            Add-Content -Path $filepath -Value "#> <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value "[CmdletBinding(SupportsShouldProcess, ConfirmImpact = `"Low`")] <br>" ;
            Add-Content -Path $filepath -Value "param ( <br>" ;
            Add-Content -Path $filepath -Value "[Parameter(Mandatory=`$true,Position=0,ParameterSetName='ParameterName01',HelpMessage='Helpmessage01')] <br>" ;
            Add-Content -Path $filepath -Value "[string]`$parameter01 , <br>" ;
            Add-Content -Path $filepath -Value "[Parameter(Mandatory=`$false,ParameterSetName='ParameterName02',HelpMessage='Helpmessage02')] <br>" ;
            Add-Content -Path $filepath -Value "[switch]`$parameter02 <br>" ;
            Add-Content -Path $filepath -Value ") <br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value "<br>" ;
            Add-Content -Path $filepath -Value "} <br>" ;
            Add-Content -Path $filepath -Value "</p> <br>"
            Add-Content -Path $filepath -Value "<br>"
            Add-Content -Path $filepath -Value "</body>"
            Add-Content -Path $filepath -Value "</html>"    
        } else {
            Write-Host "function [Name] {" ;
            Write-Host "<#" ;
            Write-Host ".SYNOPSIS" ;
            Write-Host "" ;
            Write-Host ".DESCRIPTION" ;
            Write-Host "" ;
            Write-Host ".EXAMPLE" ;
            Write-Host "" ;
            Write-Host ".INPUTS" ; 
            Write-Host "" ;
            Write-Host ".OUTPUTS" ;
            Write-Host "" ;
            Write-Host ".NOTES" ;
            Write-Host "" ;
            Write-Host "Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute" ;
            Write-Host "Author: Ineluki" ;
            Write-Host "Message: GUI fails, Powershell prevails!" ;
            Write-Host "#>" ;
            Write-Host "" ;
            Write-Host "[CmdletBinding(SupportsShouldProcess, ConfirmImpact = `"Low`")]" ;
            Write-Host "param (" ;
            Write-Host "[Parameter(Mandatory=`$true,Position=0,ParameterSetName='ParameterName01',HelpMessage='Helpmessage01')]" ;
            Write-Host "[string]`$parameter01 ," ;
            Write-Host "[Parameter(Mandatory=`$false,ParameterSetName='ParameterName02',HelpMessage='Helpmessage02')]" ;
            Write-Host "[switch]`$parameter02" ;
            Write-Host ")" ;
            Write-Host "" ;
            Write-Host "" ;
            Write-Host "" ;
            Write-Host "}" ;
        } 
           
}