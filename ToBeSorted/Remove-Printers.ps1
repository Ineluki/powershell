$printserver = "" ;

$printers = Get-WmiObject -Class Win32_Printer ;

foreach ($printer in $printers) {
    <# $printer is the current item #>
    if (($printer.Systemname) -eq $printserver) {
        <# Action to perform if the condition is true #>
        (New-Object -ComObject WScript.Network).RemovePrinterConnection($($printer.Name))
    }
}