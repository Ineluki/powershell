function Get-SQLServerVersion {
    <#
    .SYNOPSIS
        Creates an array of PsCustomOjects, containing data about SQL instances, mainly versions.

    .DESCRIPTION
        Get-SQLServerVersion is a function that creates an PSCustomObject-Array with useful information on SQL server instances.

    .EXAMPLE
        $SQLServerInstanceReport = Get-SQLServerVersion ; Export-CSV -Delimiter ";" -NoTypeInformation -Path "C:\Temp\sql_report.csv" ;

    .INPUTS
        none

    .OUTPUTS
        PSCustomObject

    .NOTES
        Important: On-premises SQL Server only!
        Author: Ineluki
        Message: GUI fails, Powershell prevails!
    #>
    
    #Declare an array for the pscustomobjects
    $result = @() ;

    #Get all sql instances from registry
    $instances_list = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server').InstalledInstances ;
    
    #Loop through every instance and collect information
    foreach ($instance in $instances_list) {

        $instance_fullname = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL').$instance ;
        $sqlprogramdir = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$instance_fullname\Setup').SqlProgramDir ;
        $sqlbinroot = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$instance_fullname\Setup').SqlBinRoot ;
        $edition = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$instance_fullname\Setup').Edition ;
        $editiontype = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$instance_fullname\Setup').EditionType ;
        $version = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$instance_fullname\Setup').Version ;
        $patchlevel = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server\$instance_fullname\Setup').PatchLevel ;

        $majorversion = switch -Regex ($version) {
            '8' {'SQL2000'}
            '9' {'SQL2005'}
            '10.0' {'SQL2008'}
            '10.5' {'SQL2008 R2'}
            '11' {'SQL2012'}
            '12' {'SQL2014'}
            '13' {'SQL2016'}
            '14' {'SQL2017'}
            '15' {'SQL2019'}
            Default {"Unknown Version!"}
        }
        #Write the information to a PsCustomObject
        $instance_summary = [PSCustomObject]@{
            'Instance_Fullname' = $instance_fullname ;
            'SQLProgramDir' = $sqlprogramdir ;
            'SQLBinRoot' = $sqlbinroot ;
            'Edition' = $edition ;
            'EditionType' = $editiontype ;
            'Version' = $version ;
            'PatchLevel' = $patchlevel ;
            'MajorVersion' = $majorversion ;
        }
        #Add the object to the array
        $result += $instance_summary ;       
    }
    return $result ;
}