$outlook_version = (Get-ItemProperty "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Office\16.0\Outlook" -Name Bitness).Bitness ;
if ($outlook_version -eq "x86") {
    <# Action to perform if the condition is true #>
    Write-Host "Outlook 32-Bit" -ForegroundColor Magenta -BackgroundColor White ;
} else {
    <# Action when all if and elseif conditions are false #>
    Write-Host "Outlook 64-Bit" -ForegroundColor Magenta -BackgroundColor White ;
}