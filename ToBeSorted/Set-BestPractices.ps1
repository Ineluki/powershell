$smbserver_config = Get-SmbServerConfiguration ;
$smbclient_config = Get-SmbClientConfiguration ;

$smbserver_config | Select-Object -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "C:\Temp\smbserver_config.csv" ;
$smbclient_config | Select-Object -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "C:\Temp\smbclient_config.csv" ;

#Short file name creation
Set-Itemproperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Fileystem" -Name "NtfsDisable8dot3NameCreation" -Value "1" ;
#SMB server
Set-SmbServerConfiguration -MaxMpxCount 50 ;
Set-SmbServerConfiguration -MaxWorkItems 1 ;
Set-SmbServerConfiguration -AutoDisconnectTimeout 0 ;
Set-SmbServerConfiguration -AsynchronousCredits 64 ;
Set-SmbServerConfiguration -Smb2CreditsMin 128 ;
Set-SmbServerConfiguration -Smb2CreditsMax 2048 ;
Set-SmbServerConfiguration -DurableHandleV2TimeoutInSeconds 30 ;
Set-SmbServerConfiguration -CachedOpenLimit 5 ;
#SMB client
Set-SmbClientConfiguration -SessionTimeout 45 ;
Set-SmbClientConfiguration -DirectoryCacheLifetime 10 ;
Set-SmbClientConfiguration -FileNotFoundCacheLifetime 5 ;
Set-SmbClientConfiguration -MaxCmds 50 ;
Set-SmbClientConfiguration -FileInfoCacheLifetime 10 ;
#Srv.sys should start on demand - CAN BREAK/PREVENT SMBv1 connections
#Via CMD: sc config srv start=demand 
Get-CimInstance -ClassName CIM_Service -Filter "Name='srv'" | Invoke-CimMethod -MethodName ChangeStartMode -Arguments @{StartMode='Manual'} ;