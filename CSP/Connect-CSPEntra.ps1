#CSP user userprincipalname, e.g. jason.gilbert@testorgcsp.onmicrosoft.com
$upn = "" ;
#Name of the organization you want to connect to, e.g. "customerdomain.com"
$delegatedorg = "" ;
#ID of the Azure Tenant of "customerdomain.com"
$tenantid = "" ;

Connect-AzureAd -TenantID $tenantid ;
