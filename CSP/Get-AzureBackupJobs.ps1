#CSP user userprincipalname, e.g. jason.gilbert@testorgcsp.onmicrosoft.com
$upn = "jason.gilbert@testorgcsp.onmicrosoft.com" ;
#Name of the organization you want to connect to, e.g. "customerdomain.com"
$delegatedorg = "customerdomain.com" ;
#ID of the Azure Tenant of "customerdomain.com"
$tenantid = "ID" ;

Connect-AzAccount -Tenant $tenantid ;
#Show all recoveryvaults 
Get-AzRecoveryServicesVault ;
#Set the chosen context
Get-AzRecoveryServicesVault | Set-AzRecoveryServicesVaultContext ;

Get-AzRecoveryServicesBackupJob ;