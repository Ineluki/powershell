#Cleanup logs older than this value will be removed
$days = 14 ;

#Log paths - adjust manually if neccessary
$iis_logpath = (Get-Website -Name "Default Web Site").logFile.Directory ;

if ($iis_logpath -eq "%SystemDrive%\inetpub\logs\LogFiles") {
    <# Action to perform if the condition is true #>
    $iis_logpath = $env:SystemDrive ;
    $iis_logpath += "\inetpub\logs\LogFiles" ;
}

#Set the paths for log cleanup
$exchange_installpath = $env:ExchangeInstallPath ;

$exchange_logpath = $env:ExchangeInstallPath ;
$exchange_logpath += "Logging" ;

$exchange_logpath_search_etltrace = $exchange_installpath ;
$exchange_logpath_search_etltrace += "Bin\Search\Ceres\Diagnostics\ETLTraces\"

$exchange_logpath_search_logs = $exchange_installpath ;
$exchange_logpath_search_logs += "Bin\Search\Ceres\Diagnostics\Logs\" ;

$exchange_logpath_unifiedcontent = $exchange_installpath ;
$exchange_logpath_unifiedcontent += "TransportRoles\data\Temp\UnifiedContent" ;

#Functions to cleanup logfiles older then set number of days
function Remove-LogFiles {
    param (
        $Targetfolderpath
    )
    
    if (Test-Path -Path $targetfolderpath) {
        <# Action to perform if the condition is true #>
        $removeuntil = (Get-Date).AddDays(-$days) ;

        $filestoremove = Get-ChildItem -Path $targetfolderpath -Recurse | Where-Object {$_.Name -like "*.log" -or $_.Name -like "*.blg" -or $_.Name -like "*.etl"} | Where-Object {$_.LastWriteTime -le $removeuntil} ;
        $filestoremove | Remove-Item -Force -ErrorAction SilentlyContinue ;
        Return
    }
}

function Remove-UnifiedContent {
    param (
        $Targetfolderpath
    )
    
    if (Test-Path -Path $targetfolderpath) {
        <# Action to perform if the condition is true #>
        $removeuntil = (Get-Date).AddDays(-$days) ;

        $filestoremove = Get-ChildItem -Path $targetfolderpath -Recurse | Where-Object {$_.LastWriteTime -le $removeuntil} ;
        $filestoremove | Remove-Item -Force -ErrorAction SilentlyContinue ;
        Return
    }
}

#Delete logfiles
Remove-LogFiles -Targetfolderpath $iis_logpath ;
Remove-LogFiles -Targetfolderpath $exchange_logpath ;
Remove-LogFiles -Targetfolderpath $exchange_logpath_search_etltrace ;
Remove-LogFiles -Targetfolderpath $exchange_logpath_search_logs ;
Remove-UnifiedContent -Targetfolderpath $exchange_logpath_unifiedcontent ;