#OnPremises Exchange servers only
#Export MessageTrackingLogs into CSV file

<#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>

#Set the scope of commandlets to entire forest
Set-AdServerSettings -ViewEntireForest $true ;

##Setting some variables to filter messages
$server = $env:COMPUTERNAME ;
$subject = "Please give away your Login to Hackers" ;

#Example of getting messages of the last 30 minutes: $start = ((Get-Date).AddMinutes(-30))
$start = "09/27/2021 08:00" ;
$end = "09/27/2021 17:00" ;

#Generate the absolute path for the results file
##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
}

#Create timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

#Create the absolute path for the results file
$result_exportpath = "C:\Temp\" ; 
$result_exportpath += $server ;
$result_exportpath += "_Ticketnummer_" ;
$result_exportpath += $timestamp ;
$result_exportpath += ".csv" ;

#Get the log, filter and export it
$MessageTrackingLog = Get-MessageTrackingLog -ResultSize Unlimited -Start $start -End $end -MessageSubject $subject ;
$MessageTrackingLog | Select-Object TimeStamp,EventId,Sender,@{Name="Recipients";Expression={$_.recipients -join ","}} | Export-Csv -Delimiter ";" -NoTypeInformation -Path $result_exportpath ;
