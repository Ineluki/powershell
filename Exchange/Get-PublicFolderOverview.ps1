function Get-PublicFolderOverview {
    <#
    .SYNOPSIS
        Creates a list of all Exchange public folders and their access rights
    .DESCRIPTION
        Get-PublicFolderOverview is a function that does create an overview of all Exchange public folders and their ACLs.
    .EXAMPLE
        Get-PublicFolderOverview -ResultExportPath "C:\Temp\pfoverview.csv" 
    .INPUTS
        None
    .OUTPUTS
       None or CSV file 
    .NOTES
        Important: 1. Understand -> 2. Adjust/Improve -> 3.Execute
        Author: Ineluki
        Message: GUI fails, Powershell prevails!
    #>
    
    [CmdletBinding(ConfirmImpact = "Low")]
    param (
    [Parameter(Mandatory=$false,Position=0,ParameterSetName='ResultExportPath',HelpMessage='Location where the result will be saved as CSV file. Access rights and absolute path required!')]
    [string]$ResultExportPath
    )
    
    <#Ensure that Exchange commandlets are available
    #Load PSSnapin for Exchange2007
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
    #Load PSSnapin for Exchange2010
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
    #Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
    #>
    #Set the scope of commandlets to entire forest
    Set-AdServerSettings -ViewEntireForest $true ;

    #Declare array for later use
    $pfperm = @() ;

    #Save all public folder to variable 
    $pfsum = Get-PublicFolder -Recurse -ResultSize Unlimited ;

    #Process each public folder and get ACL
    foreach ($pf in $pfsum) {
        #Save permissions to variable
        $pfperm += $pf | Get-PublicFolderClientPermission ;
    }
    
    #Check if parameter 'ResultExportPath' was set and export to CSV, return results to console if it wasn't
    if ($PSBoundParameters.ContainsKey('ResultExportPath')) {   
        $pfperm | Select-Object Foldername,User,@{Name="AccessRights";Expression={$_.AccessRights -join ","}} | Export-Csv -Path $ResultExportPath -NoTypeInformation -Encoding "UTF8" -Delimiter ";" ;  
    } else {
        Return $pfperm
    }

    Return 
}