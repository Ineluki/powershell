#Filter events in eventlog to get IP adresses trying to connect to Exchange
#Get alle the relevant event entries and export them to a ccsv file
$winevents = "C:\Temp\securityincient_events.csv" ;
Get-WinEvent -ProviderName MSExchangeFrontEndTransport | Where-Object {$_.Id -eq "1035"} | Select-Object TimeCreated,LogName,ProviderName,TaskDisplayName,Message | Export-Csv -NoTypeInformation -Encoding UTF8 -Delimiter ";" -Path $winevents ;

#Import the csv file 
$winevent_list= Import-Csv -Delimiter ";" -Path $winevents ;
$threatlist = @() ;

foreach ($entry in $winevent_list) {
    <# $entry is the current item #>
    [string]$message = ($entry.message) ;

    $ip = $message.Split('[',2) ;
    $ip = $ip[1].Replace("].","") ;

    $threat = [PSCustomObject]@{
        'IP-Address' = $ip
    }
    $threatlist += $threat ;
}
$threatlist ;
