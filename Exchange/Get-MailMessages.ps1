#Collect infomration from messagetrackinglog on Exchange server, format and export it

<#
#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>

#Set the scope of commandlets to entire forest
Set-AdServerSettings -ViewEntireForest $true ;

#Provide ticketnumber for filename
$ticketnumber = "" ;

#Generate the absolute path for results file
##Check if C:\Temp exist, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -Itemtype Directory -Path "C:\Temp" ;
}

#Create timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

#Get servername
$server = $env:computername ;

#Create the absolute path
$result_exportpath = "C:\Temp\" ;
$result_exportpath += $server ;
$result_exportpath += "_" ;
$result_exportpath += $ticketnumber ;
$result_exportpath += "_" ;
$result_exportpath += $timestamp ;
$result_exportpath += ".csv" ;

#Declaring an array for later use
$result = @() ;

#Define start and end of message search
#Example: $start = ((Get-Date).AddMinutes(-30))
$start = "01/01/2021 00:01" ;
$end = "09/30/2021 23:59" ;

#Get all mailboxes to search through them 
$mailbox_list = Get-Mailbox ;

#Get the MTL, filter it and export results
foreach ($mailbox in $mailbox_list) {
    
    $messagetrackinglog =  Get-MessageTrackingLog -ResultSize Unlimited -Start $start -End $end -Recipients $mailbox -EventId RECEIVE ;
    $messagesize_total = 0 ;
    $message_count = 0 ;

    foreach ($message in $messagetrackinglog) {
        $messagesize_total += ($message.TotalBytes / 1MB) ;
        $message_count += 1 ;        
    }

    $messagesize_total = [math]::Round($messagesize_total, 2) ;

    $mailbox_result_overview = [PSCustomObject]@{
        PrimarySmtpAddress = $mailbox ;
        TotalSizeofMessagesinMB = $messagesize_total ;
        TotalReceivedMessages = $message_count ;
    }
    $result += $mailbox_result_overview ;
}
$result | Export-Csv -Delimiter ";" -NoTypeInformation -Path $result_exportpath ;