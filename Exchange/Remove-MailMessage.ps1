#OnPremises Exchange only
#Delete all emails with a specific subject from all mailboxes

<#
#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>

#Set the scope of commandlets to entire forest
#Set-AdServerSettings -ViewEntireForest $true ;

#Generate the absolute path for the results file
##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
}
#Create timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;
#Create the absolute path for the results file
$result_exportpath = "C:\Temp\" ; 
$result_exportpath += $server ;
$result_exportpath += "_Ticketnummer_" ;
$result_exportpath += $timestamp ;
$result_exportpath += ".csv" ;

##Generate a list of all mailboxes
$mailbox_list = Get-Mailbox -ResultSize Unlimited ;
##Declare an array for the results
$result = @() ;

#Loop through the list and delete messages with a specific subject
foreach ($mailbox in $mailbox_list) {
    $documented_query = Search-Mailbox -Id $mailbox -DeleteContent -Force -Confirm:$false -SearchQuery 'subject:MyBadPhisingEmailSubject' ;
    $result += $documented_query | Select-Object Identity,Success,ResultItemsCount,ResultItemsSize ;
}
#Export results
$result | Export-Csv -Delimiter ";" -NoTypeInformation -Path $result_exportpath ;