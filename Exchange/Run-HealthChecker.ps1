#Download&execute Microsoft HealthChecker script for Exchange Server

##Remove old HealthChecker files
if ((Test-Path -Path "C:\Temp\HealthChecker") -eq $true) {
    Get-ChildItem -Path "C:\Temp\HealthChecker" | Remove-Item ;
}
##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
} elseif ((Test-Path -Path "C:\Temp\HealthChecker") -ne $true)  {
    New-Item -ItemType Directory -Path "C:\Temp\HealthChecker" ;  
}

##Set variables for the download command
$source = "https://github.com/microsoft/CSS-Exchange/releases/latest/download/HealthChecker.ps1" ;
$destination = "C:\Temp\HealthChecker\HealthChecker.ps1" ;

##Start the download
Start-BitsTransfer -Source $source -Destination $destination ;

##Unblockfile&execute it
Unblock-File -Path $destination ;
Set-Location -Path "C:\Temp\HealthChecker" ;
.\HealthChecker.ps1 ;