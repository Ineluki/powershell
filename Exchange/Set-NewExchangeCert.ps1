
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#Set the scope of commandlets to entire forest
Set-AdServerSettings -ViewEntireForest $true ;

##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
}
#Create a timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

##Create a new folder
$newfolder_name = "NewExchangeCert-" ;
$newfolder_name += $timestamp ;
$newfolder_path = "C:\Temp\" ;
$newfolder_path += $newfolder_name ;

if ((Test-Path -Path $newfolder_path) -ne $true) {
    New-Item -ItemType Directory -Path $newfolder_path ;   
}

#Collect data
$exchangecerticates = Get-ExchangeCertificate  ;
$exchangecerticates | Format-Table Subject,Thumbprint ;
$receiveconnectors = Get-ReceiveConnector  ;
$receiveconnectors | Format-List Identity,*TLS* ;
$sendconnectors = Get-SendConnector ;
$sendconnectors | Format-List Identity,*TLS* ;

#Change to work directory
Set-Location -Path $newfolder_path ;

#Save data for later purposes
$exchangecerticates | Select-Object * | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "$newfolder_path\certificates_prechange.csv" ;
$receiveconnectors | Select-Object * | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "$newfolder_path\receiveconnectors_prechange.csv" ;
$sendconnectors | Select-Object * | Export-Csv -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Path "$newfolder_path\sendconnectors_prechange.csv" ;

#Set variables for later use - IMPORTANT!
#Copy/Move the new Certificate into the folder as a prerequisite and provide the name of the PFX file.
$pfx_localfolderpath = "$newfolder_path\pfxname.pfx" ;
$services = "IIS,SMTP,IMAP,POP" ;

#Import the new certificate - local path for import possible!
$import = Import-ExchangeCertificate -FileName $pfx_localfolderpath -Password ((Get-Credential).Password) ;

#Get "new" certificate via Thumbprint
$thumbprint_new = ($import.Thumbprint) ;
$cert_new = Get-ExchangeCertificate -Thumbprint $thumbprint_new ;
$tlscertname = "<i>$($cert_new.Issuer)<s>$($cert_new.Subject)" ;

#Enable new certificate for services
$cert_new | Enable-ExchangeCertificate -Services $services ;

#Bind new cert to receiveconnectors
Get-ReceiveConnector | Format-List Identity,*tls* ;

$receivecon = "" ;
Get-ReceiveConnector $receivecon | Set-ReceiveConnector -TlsCertificateName $tlscertname ;

#Bind new cert to sendconnectors
Get-ReceiveConnector | Format-List Identity,*tls* ;

$sendcon = "" ;
Get-SendConnector $sendcon | Set-SendConnector -TlsCertificateName $tlscertname ;

#Reset Internet Information Services (IIS)
iisreset ;

#Remove old cert
$thumbprint_old = "" ;
Remove-ExchangeCertificate -Thumbprint $thumbprint_old ;
