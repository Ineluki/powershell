<#
#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>

#Set the scope of commandlets to entire forest
Set-AdServerSettings -ViewEntireForest $true ;

#Declare variables for later use
$errors_policy = @() ;
$errors_alias = @() ;

$distributiongroupalias_list = (Get-DistributionGroup -ResultSize Unlimited).Alias | Get-Unique ;

foreach ($distributiongroupalias in $distributiongroupalias_list) {
    #Search for each individual group to check if there are multiple results for the samee alias
    $multiplealias_test = Get-DistributionGroup $distributiongroupalias -ResultSize Unlimited ;
    #Check if there is more than one result
    if (($multiplealias_test.Count) -gt 1) {
        $aliascounter = 0 ;

        #Process each distributiongroup with alias conflicts
        foreach ($multiplealias in $multiplealias_test) {
            #Naming scheme for new aliases "oldalias_$aliascounter"
            $aliascounter ++ ;
            $newalias = $multiplealias.Alias ;
            $newalias += "_" ;
            $newalias += $aliascounter ;

            #Upgrade the version of the distributiongroup to avoid issues
            $multiplealias | Set-DistributionGroup -ForceUpgrade ;

            #Deactivate auto-update of SMTP addresses via address policy
            #Message variable for console output
            $message = "" ;
            try {
                $multiplealias | Set-DistributionGroup -EmailAddressPolicyEnabled $false ;
                $errors_rename = $false ;
                $message = "Email address policy forced auto update disabled for " ;
                $message += ($multiplealias.Name) ;
                Write-Host $message -ForeGroundColor Green ;
            }
            #Skip renaming of alias if address policy auto-update of SMTP address couldn't be disabled
            catch {
                $errors_rename = $true ;
                $errors_policy += $multiplealias.Name ;
                $message = "Error disabling email address auto update policy for " ;
                $message += ($multiplealias.Name) ;
                Write-Host $message -ForeGroundColor Magenta -BackgroundColor White ;                
            }
            if ($errors_rename -eq $false ) {
            #Message2 variable for console output
            $message2 = "" ;
                try {
                    $multiplealias | Set-DistributionGroup -Alias $newalias -IgnoreNamingPolicy ;
                    $message2 = "Successfully changed alias for " ;
                    $message2 += ($multiplealias.Name) ;
                    Write-Host $message2 -ForeGroundColor Green ;
                }
                catch {
                    $errors_alias += $multiplealias.Name ;
                    $message = "Error changing alias for " ;
                    $message += ($multiplealias.Name) ;
                    Write-Host $message2 -ForeGroundColor Magenta -BackgroundColor White ;
                }
                   
            }
            
        }
    }
}
$errors_policy ;
$errors_alias ;