function Get-ExchangeVirtualDirectories{
<#
.SYNOPSIS
    Creates an array of PsCustomObjects, containing on-premise Exchange virtual directory URLs.

.DESCRIPTION
    Get-ExchangeVirtualDirectories is a function that creates an pscustomject array with all the configured URLs of virtual directories.

.EXAMPLE
    $exchangeURLs = Get-ExchangeVirtualDirectories ; $exchangeURLs | Export-Csv -Path C:\Temp\exchange_urls.csv -Delimiter ";" -NoTypeInformation

.INPUTS
    none

.OUTPUTS
    PSCustomObject

.NOTES
    Important: On-premises Exchange Server only!
    Author: Ineluki
    Message: GUI fails, Powershell prevails!
#>

    <#Ensure that Exchange commandlets are available
    #Load PSSnapin for Exchange2007
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
    #Load PSSnapin for Exchange2010
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
    #Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
    #>
    #Set the scope of commandlets to entire forest
    Set-AdServerSettings -ViewEntireForest $true ;

    #Query AD for all Exchange servers in Exchange Organization
    $exservers = Get-ExchangeServer ;

    #Declare variable for later use
    $exchangeserversvirtualdirectoryconfig = @() ;

    #Collect information about each server and generate a corresponding pscustomobject
    foreach ($exserver in $exservers) {

        #Save URLs to variables for later use
        $owa = Get-OwaVirtualDirectory -Server $exserver
        $ecp = Get-EcpVirtualDirectory -Server $exserver
        $autodiscover = Get-ClientAccessService -Server $exserver
        $oab = Get-OABVirtualDirectory -Server $exserver
        $activesync = Get-ActiveSyncVirtualDirectories -Server $exserver
        $ews = Get-WebServicesVirtualDirectories -Server $exserver
        $powershell = Get-PowerShellVirtualDirectory -Server $exserver
        $outlookanywhere = Get-OutlookAnywhere -Server $exserver

        #Generate a pscustomobject and fill it with information
        $exserverurlconfig = [PSCustomObject]@{
            'ExchangeServer' = $exserver.Name
            'OwaInternalUrl' = $owa.InternalUrl
            'OwaExternalUrl' = $owa.ExternalUrl
            'EcpInternalUrl' = $ecp.InternalUrl
            'EcpExternalUrl' = $ecp.ExternalUrl
            'AutodiscoverURI' = $autodiscover.AutoDiscoverServiceInternalUri
            'OABInternalUrl' = $oab.InternalUrl
            'OABExternalURL' = $oab.ExternalUrl
            'ActiveSyncInternalUrl' = $activesync.InternalUrl
            'ActiveSyncExternalUrl' = $activesync.ExternalUrl
            'OutlookAnywhereInternalHostname' = $outlookanywhere.InternalHostname
            'OutlookAnywhereExternalHostname' = $outlookanywhere.ExternalHostname
            'PowershellInternalUrl' = $powershell.InternalUrl
            'PowershellExternalUrl' = $powershell.ExternalUrl
            'EWSInternalUrl' = $ews.InternalUrl
            'EWSExternalUrl' = $ews.ExternalUrl
        }
        #Write the pscustomoject into an array
        $exchangeserversvirtualdirectoryconfig += $exserverurlconfig
    }
    #Return the array as output
    return $exchangeserversvirtualdirectoryconfig 
}