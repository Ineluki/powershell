#Can solve IDFix errors where DisplayName field in AD is marked as "Blank"
<#
#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>

#Set the scope of commandlets to entire forest
Set-AdServerSettings -ViewEntireForest $true ;

#Set variables for later use
$error_updatestoremailboxstate = @() ;
#Get all Exchange databases
$exdb_list = Get-MailboxDatabase ;

foreach ($exdb in $exdb_list) {

   $mbstats_list = Get-Mailboxstatistics -Database $exdb ;
   
   foreach ($mbstats in $mbstats_list) {
       try {
           Update-StoreMailboxState -Database ($mbstats.Database) -Identity ($mbstats.MailboxGuid) -Confirm:$false ;
       }
       catch {
           $error_updatestoremailboxstate += ($_.ToString()) ;
       }
   }
}
$error_updatestoremailboxstate ;