$mb_list = Get-Mailbox -ResultSize Unlimited ;
$calendarpermissionssummary = @() ;

foreach ($mb in $mb_list) {
    <# $mb is the current item #>
    $mb_perm = Get-MailboxFolderPermission -Identity "${mb}:\Kalender" ;
    $calendarpermissions = [PSCustomObject]@{
        'Mailbox' = $mb.PrimarySMTPAddress 
        'User' = $mb_perm[0].User
        'AccessRights' = $mb_perm[0].AccessRights
    }
    $calendarpermissionssummary += $calendarpermissions ;
}
$calendarpermissionssummary ;