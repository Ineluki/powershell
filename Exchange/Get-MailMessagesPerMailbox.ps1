#Get emails for a specific timespan and compute the size total

<#
#Ensure that Exchange commandlets are available
#Load PSSnapin for Exchange2007
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
#Load PSSnapin for Exchange2010
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
#Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
#>

#Set the scope of commandlets to entire forest
Set-AdServerSettings -ViewEntireForest $true ;

#Generate the absolute path for the results file
##Check if C:\Temp exists, create it otherwise
if ((Test-Path -Path "C:\Temp") -ne $true) {
    New-Item -ItemType Directory -Path "C:\Temp" ;   
}

#Create timestamp
$timestamp = Get-Date -Format "ddMMyyyy_Hmmss" ;

#Get servername
$server = $env:COMPUTERNAME ;

#Create the absolute path for the results file
$result_exportpath = "C:\Temp\" ; 
$result_exportpath += $server ;
$result_exportpath += "_Ticketnummer_" ;
$result_exportpath += $timestamp ;
$result_exportpath += ".csv" ;

#Declaring an array for results
$result = @() ;

#Example of getting messages of the last 30 minutes: $start = ((Get-Date).AddMinutes(-30))
$start = "09/27/2021 08:00" ;
$end = "09/27/2021 17:00" ;

#Set the path to the CSV file containing the mailboxes
$csv_path = "C:\Temp\" ;
$mailbox_list = Import-Csv -Path $csv_path ;

#Get the log, filter and export it
foreach ($mailbox in $mailbox_list) {

    #Get the messages from Exchange Server MessageTrackingLog
    $messagetrackinglog = Get-MessageTrackingLog -ResultSize Unlimited -Start $start -End $end -Recipients $mailbox -EventId RECEIVE ;

    foreach ($message in $messagetrackinglog) {

        #Get the total size of all messages
        $messagesize_total += $message.TotalBytes ;
    }

    #Format totalsize to Megabytes and round it
    $messagesize_total = $messagesize_total / 1KB ;
    $messagesize_total = $messagesize_total / 1MB ;
    $messagesize_total = [math]::Round($messagesize_total) ;

    #Create a PsCustomObject per mailbox and fill it with information
    $mailbox_messages_totalsize = [PSCustomObject]@{
        'PrimarySmtpAddress' = $mailbox ;
        'TotalSizeOfMessagesInGivenTimeFrame' = $messagesize_total ;
    }

    #Add the object to an array
    $result += $mailbox_messages_totalsize ;

}

#Export the result in a CSV file
$result | Export-Csv -Delimiter ";" -NoTypeInformation -Path $result_exportpath ;
