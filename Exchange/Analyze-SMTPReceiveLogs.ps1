
#Provide some information and save them to variables for later use
$logfile_path = $env:ExchangeInstallPath ;
$logfile_path += "TransportRoles\Logs\FrontEnd\ProtocolLog\SmtpReceive\*.log" ;
$result_summary = "C:\Temp\IPAddresses.txt" ;

#Get a list of the log files
$logfiles = Get-ChildItem $logfile_path ;

# Initializes an array for the results
$ips = foreach ($log in $logfiles) {

    #Write progress information to console
    $percent_progress = [int](($logfiles.IndexOf($log) + 1) / $count * 100) ;
    Write-Progress -Activity "Collecting Log details" -Status $status -PercentComplete $percent_progress ;
    Write-Host "Processing Log File $($log.FullName)" -ForegroundColor Magenta -BackgroundColor White ;

    # Reads the content of the log file, skipping the first five lines
    $fileContent = Get-Content $log | Select-Object -Skip 5 ;

    foreach ($line in $fileContent) {

        # Extracts the IP address from the socket information in the log line
        $socket = $line.Split(',')[5]
        $ip = $socket.Split(':')[0]

        # Adds the IP address to the array
        $ip
    }
}

# Removes duplicate IP addresses from the $ips array and sorts them alphabetically
$uniqueips = $ips | Select-Object -Unique | Sort-Object

# Displays the list of unique IP addresses on the console
Write-Host "List of IPs:" ;
$uniqueips ;
Write-Host "" ;

# Writes the list of unique IP addresses to the output file
$uniqueIps | Out-File $result_summary ;