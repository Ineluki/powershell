function Get-MailboxOnPremReport{
    <#
    .SYNOPSIS
        Creates an array of PsCustomObjects, containing on-premise mailbox data .
    
    .DESCRIPTION
        Get-MailboxOnPremReport is a function that creates an pscustomject array with useful information on mailboxes.
    
    .EXAMPLE
        $OnPremReport = Get-MailboxOnPremReport ; $OnPremReport | Export-Csv -Path C:\Temp\mailbox_report.csv -Delimiter ";" -NoTypeInformation
    
    .INPUTS
        none
    
    .OUTPUTS
        PSCustomObject
    
    .NOTES
        Important: On-premises Exchange Server only!
        Author: Ineluki
        Message: GUI fails, Powershell prevails!
    #>

    <#Ensure that Exchange commandlets are available
    #Load PSSnapin for Exchange2007
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.Admin ;
    #Load PSSnapin for Exchange2010
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.E2010 ; 
    #Load PSSnapin for Exchange2013, Exchange2016 or Exchange2019
    Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn ;
    #>
    #Set the scope of commandlets to entire forest
    Set-AdServerSettings -ViewEntireForest $true ;

    #Query AD for all Exchange servers in Exchange Organization
    $mailboxes = Get-Mailbox -ResultSize Unlimited ;
    
    #Declare variable for later use
    $mailboxreport = @() ;
    
    #Collect information about each server and generate a corresponding pscustomobject
    foreach ($mb in $mailboxes) {
    
    #Save statistics data to variables for later use
    $mailboxstatistics = $mb | Get-MailboxStatistics ;
    
    #Generate a pscustomobject and fill it with information
    $mailboxsummary = [PSCustomObject]@{
        'PrimarySmtpAddress' = $mb.PrimarySmtpAddress
        'Name' = $mb.Name
        'Identity' = $mb.Identity
        'Alias' = $mb.Alias
        'UserPrincipalName' = $mb.UserPrincipalName
        'AuditEnabled' = $mb.AuditEnabled
        'HiddenFromAddressListsEnabled' = $mb.HiddenFromAddressListsEnabled
        'MaxSendSize' = $mb.MaxSendSize
        'MaxReceiveSize' = $mb.MaxReceiveSize
        'IssueWarningQuota' = $mb.IssueWarningQuota
        'ProhibitSendQuota' = $mb.ProhibitSendQuota
        'ProhibitSendReceiveQuota' = $mb.ProhibitSendReceiveQuota
        'DeliverToMailboxAndForward' = $mb.DeliverToMailboxAndForward
        'ForwardingAddress' = $mb.ForwardingAddress
        'ForwardingSmtpAddress' = $mb.ForwardingSmtpAddress
        'EmailAddresses' = ($mb.EmailAddresses -join ",") 
        'TotalItemSize' = $mailboxstatistics.TotalItemSize
        'ItemCount' = $mailboxstatistics.ItemCount
        'IsQuarantined' = $mailboxstatistics.IsQuarantined
        'LastLogonTime' = $mailboxstatistics.LastLogonTime
        'DisconnectDate' = $mailboxstatistics.DisconnectDate
    }
    #Write the pscustomoject into an array
    $mailboxreport += $mailboxsummary ;
    }
    #Return the array as output
    return $mailboxreport 
}